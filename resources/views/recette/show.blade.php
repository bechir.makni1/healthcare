@extends('layouts.admin.app')
@section('title', "liste des recettes")
@section('dashboard-header')
<br/>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Details de recette</h4>
        </div>
        @endsection
        @section('dashboard-content')
            <div class="row mt-3">
                <div class="col-md-8">
                    <div class="blog-view">
                        <article class="blog blog-single-post">
                            <h3 class="blog-title">{{$recette->title}}</h3>
                            <div class="blog-image">
                                <a href="blog-details.html"><img alt="" src="{{ asset($recette->image)}}"
                                                                 class="img-fluid mt-4"></a>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>Description: </b></h4>
                                <p>{{$recette->description}}</p>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>Ingrédient: </b></h4>
                                <p>{{$recette->ingrédient}}</p>
                            </div>
                        </article>
                        <div class="widget">
                            @foreach($recette->tags as $tag)
                                <label class="label label-info btn btn-primary">{{ $tag->name }}</label>
                            @endforeach
                        </div>
                        <div class="widget author-widget clearfix">
                            <h3>A propos de l'auteur</h3>
                            <div class="about-author">
                                <div class="about-author-img">
                                    <div class="author-img-wrap">
                                        <img class="img-fluid" alt="" src="{{ asset($recette->user->avatar) }}">
                                    </div>
                                </div>
                                <div class="author-details"><span
                                        class="blog-author-name">{{$recette->user->fullName()}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
