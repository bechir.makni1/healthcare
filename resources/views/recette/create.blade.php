@extends('layouts.admin.app')
@section('title', "liste des recettes")
@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Ajouter une recette</h3> </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{route("recette.store")}}"  method="POST" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row formtype">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Titre de l'article</label>
                            <input
                                class="form-control"
                                type="text"
                                name="title"
                                value=""
                            />
                            @error('title')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Categorie</label>
                            <select class="form-control" id="sel1" name="category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input
                                    type="file"
                                    class="custom-file-input"
                                    id="customFile"
                                    name="image"
                                />
                                <label class="custom-file-label" for="customFile"
                                >Choisir une image</label
                                >
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                     <div class="form-group">
                        <label>ingrédient</label>
                      <textarea
                        class="form-control"
                        rows="4"
                        id="comment"
                        name="ingrédient">
                      </textarea>
                         @error('ingrédient')
                         <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                         @enderror
                     </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                    <textarea
                        class="form-control"
                        rows="5"
                        id="comment"
                        name="description">
                  </textarea>
                            @error('description')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div></div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tags</label>
                    <div class="col-md-12 mt-3">
                        <input class="form-control" type="text" data-role="tagsinput" name="tags">
                        @if($errors->has('tags'))
                            <span class="text-danger">{{ $errors->first('tags') }}</span>
                        @endif
                   </div>
                        </div></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Publication</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input  class="form-check-input" type="radio" id="article_active" name="isActive" value="1" checked>
                            <label class="form-check-label" for="article_active">Publier</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input  class="form-check-input" type="radio" id="article_inactive" name="isActive" value="0">
                            <label class="form-check-label" for="article_inactive">Ne pas publier</label>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Commentaires</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input  class="form-check-input" type="radio" id="article_comment_active" name="isComment" value="1" checked>
                            <label class="form-check-label" for="article_comment_active">Autorise</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input  class="form-check-input" type="radio" id="article_comment_inactive" name="isComment" value="0">
                            <label class="form-check-label" for="article_comment_inactive">Non autorise</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary buttonedit1">Enregistrer</button>
                </div>

            </form>
        </div>
    </div>

@endsection
