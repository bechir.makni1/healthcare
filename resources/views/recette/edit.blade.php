@extends('layouts.admin.app')

@section('title', 'Modification de recette')

@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Modifier une recette</h3>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
                <form action="{{ route('recette.update', $recette->id) }}" method="POST" enctype="multipart/form-data">

                @csrf
                @method('PUT')
                <div class="row formtype">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Titre de recette</label>
                            <input class="form-control" type="text" name="title" value="{{ old('title', $recette->title) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Categorie</label>
                            <select class="form-control" id="sel1" name="category_id">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ $recette->category_id == $category->id ? 'selected' : '' }}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="image">
                                <label class="custom-file-label" for="customFile">Choisir une image</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="5" name="description">{{ old('description', $recette->description) }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>ingrédient</label>
                            <textarea class="form-control" rows="5" name="description">{{ old('ingrédient', $recette->ingrédient) }}</textarea>
                        </div>
                    </div>
                    @if(isset($recette))
                        <div class="col-md-12 mt-3">
                            @foreach($recette->tags as $tag)
                                <label class="label label-info btn btn-primary">{{ $tag->name }}</label>
                            @endforeach
                        </div>
                    @endif
                    <div class="col-md-12 mt-3">
                        <input class="form-control" type="text" data-role="tagsinput" name="tags">
                        @if($errors->has('tags'))
                            <span class="text-danger">{{ $errors->first('tags') }}</span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Publication</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="article_active" name="is_active" value="1"{{ old('is_active', $recette->is_active) == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="article_active">Publier</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="article_inactive" name="is_active" value="0"{{ old('is_active', $recette->is_active) == 0 ? 'checked' : '' }}>
                            <label class="form-check-label" for="article_inactive">Ne pas publier</label>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Partages</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="article_share_active" name="is_shareable" value="1" {{ old('is_shareable', $recette->is_shareable) == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="article_share_active">Partageable</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="article_share_inactive" name="is_shareable" value="0" {{ old('is_shareable', $recette->is_shareable) == 0 ? 'checked' : '' }}>
                            <label class="form-check-label" for="article_share_inactive">Non Partageable</label>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Commentaires</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="article_comment_active" name="is_comment" value="1" {{ old('is_comment', $recette->is_comment) == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="article_comment_active">Autorise</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="article_comment_inactive" name="is_comment" value="0" {{ old('is_comment', $recette->is_comment) == 0 ? 'checked' : '' }}>
                            <label class="form-check-label" for="article_comment_inactive">Non autorise</label>
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary buttonedit1">Enregistrer recette</button>
            </form>
        </div>
    </div>
@endsection
