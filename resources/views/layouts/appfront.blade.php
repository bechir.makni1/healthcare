<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>HealthCare</title>
    <link href="{{asset('img/logo.png')}}" rel="shortcut icon">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Raleway:wght@600;800&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Template rating -->
        <link rel="stylesheet" type="text/css" href="path/to/starability/starability-all.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body>

<!-- Spinner Start -->
<div id="spinner"
     class="show w-100 vh-100 bg-white position-fixed translate-middle top-50 start-50  d-flex align-items-center justify-content-center">
    <div class="spinner-grow text-primary" role="status"></div>
</div>
<!-- Spinner End -->


<!-- Navbar start -->
<div class="container-fluid fixed-top">
    <div class="container topbar bg-primary d-none d-lg-block">
        <div class="d-flex justify-content-between">
            <div class="top-info ps-2">
                <small class="me-3"> 	<i class="far fa-calendar-alt me-2 text-secondary"></i><a class="text-white">@php
$time=Carbon\Carbon::now() @endphp
                    {{$time->dayName}},{{$time->isoFormat('LL')}}</a></small>
                <small class="me-3"><i class="fas fa-map-marker-alt me-2 text-secondary"></i> <a class="text-white">{{$site_settings->address}}</a></small>
                <small class="me-3"><i class="fas fa-envelope me-2 text-secondary"></i><a class="text-white">{{$site_settings->email}}</a></small>
            </div>

        </div>
    </div>
    <div class="container px-0">
        <nav class="navbar navbar-light bg-white navbar-expand-xl">
            <a class="navbar-brand"><h1 class="text-primary display-8">HealthCare</h1></a>
            <button class="navbar-toggler py-2 px-3" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarCollapse">
                <span class="fa fa-bars text-primary"></span>
            </button>
            <div class="collapse navbar-collapse bg-white" id="navbarCollapse">
                <div class="navbar-nav mx-auto">
                    <a href="{{route('welcome')}}" class="nav-item nav-link ">Accueil</a>
                    <a href="{{route('apropos')}}" class="nav-item nav-link ">À propos</a>
                    <a href="{{route('front.nutritionnistes.index')}}" class="nav-item nav-link">Nutritionnistes</a>
                    <a href="{{route('front.recettes.index')}}" class="nav-item nav-link">Recette</a>
                    <a href="{{route('conseils')}}" class="nav-item nav-link">Conseils</a>
                    <a href="{{route('shop')}}" class="nav-item nav-link">Magasin</a>
                    @if(auth()->check())
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                        <div class="dropdown-menu m-0 bg-secondary rounded-0">
                            <a href="{{route('cart.index')}}" class="dropdown-item">Panier</a>
                            <a href="{{route('front.caisse.index')}}" class="dropdown-item">Caisse</a>
{{--                            @if(auth()->check() && auth()->user()->type == \App\Models\User::TYPE_NUTRITIONNISTE)--}}
{{--                                <a href="404.html" class="dropdown-item">404 Page</a>--}}
{{--                            @endif--}}

                        </div>
                    </div>
                    @endif
                    @if(auth()->check())
                    <a href="{{route('wishlist')}}" class="nav-item nav-link">Favoris</a>
                    @endif
                </div>
                <div class="d-flex m-3 me-0">
                    <a href="{{route('cart.index')}}" class="position-relative me-4 my-auto">
                        <i class="fa fa-shopping-bag fa-2x"></i>
                        <span id="cartItemCount" class="position-absolute bg-secondary rounded-circle d-flex align-items-center justify-content-center text-dark px-1" style="top: -5px; left: 15px; height: 20px; min-width: 20px;">
    {{ count(session('cartItems') ?? []) }}
</span>


                    @if(auth()->check())
                            <a href="#" class="my-auto">
                                <ul class="nav user-menu">
                                    <li class="nav-item dropdown">
                                        <a href="#" class="dropdown-toggle" id="navbarDropdown" role="button"
                                           data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img style="height: 50px;width: 50px" class="img-fluid rounded-circle m-2"
                                                 alt=""
                                                 src="{{ auth()->user()->avatar ?  asset(auth()->user()->avatar) : asset('admin/assets/img/default-user.png') }}">

                                           <small> {{auth()->user()->fullName()}}</small>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @if(auth()->check() && (auth()->user()->type == \App\Models\User::TYPE_NUTRITIONNISTE || auth()->user()->type == \App\Models\User::TYPE_SUPER_ADMIN))
                                                <a class="dropdown-item" href="{{ route('admin.dashboard') }}">Dashboard</a>
                                            @endif
                                            @if(auth()->check() && (auth()->user()->type == \App\Models\User::TYPE_CLIENT))
                                            <a class="dropdown-item" href="{{route('client.account.edit')}}">Profile</a>
                                                @endif
                                            <a class="dropdown-item" href="{{ route('logout')}}">Se déconnecter</a>
                                        </div>
                                    </li>
                                </ul>
                            </a>

                        @else
                            <a href="{{ route('login') }}" class="position-relative me-4 my-auto btn btn-light rounded-pill bg-secondary" role="button">Login</a>
                        @endif


                    </a>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- Navbar End -->

@yield('content')
<!-- Footer Start -->
<div class="container-fluid bg-dark text-white-50 footer pt-5 mt-5">
    <div class="container py-5">
        <div class="pb-4 mb-4" style="border-bottom: 1px solid rgba(226, 175, 24, 0.5) ;">
            <div class="row g-4">
                <div class="col-lg-3">
                    <a href="#">
                        <h1 class="text-primary mb-0">{{$site_settings->web_site_name}}</h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="row g-5">
            <div class="col-lg-6 col-md-6">
                <div class="footer-item">
                    <h4 class="text-light mb-3">Pourquoi les gens aiment ! </h4>
                    <p class="mb-4"> {{Illuminate\Support\Str::limit($site_settings->about, 150) }}</p>
                    <a href="{{route('apropos')}}" class="btn border-secondary py-2 px-4 rounded-pill text-primary">En
                        savoir plus</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="d-flex flex-column text-start footer-item">
                    <h4 class="text-light mb-3">Menu</h4>
                    <a class="btn-link" href="{{route('welcome')}}"> Accueil</a>
                    <a class="btn-link" href="{{route('apropos')}}">À propos de nous</a>
                    <a class="btn-link" href="{{route('front.nutritionnistes.index')}}">Nos nutritionnistes</a>
                    <a class="btn-link" href="{{route('front.recettes.index')}}">Recettes</a>
                    <a class="btn-link" href="{{route('conseils')}}">Conseils</a>
                    <a class="btn-link" href="{{route('shop')}}">Magasin</a>

                </div>
            </div>

            <div class="col-lg-2 col-md-6">
                <div class="footer-item">
                    <h4 class="text-light mb-3">Contact</h4>
                    <p>Address: {{$site_settings->address}}</p>
                    <p>Email:{{$site_settings->email}} </p>
                    <p>Phone:{{$site_settings->phone}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->

<!-- Copyright Start -->
<div class="container-fluid copyright bg-dark py-4">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                <span class="text-light"><a href="#"><i class="fas fa-copyright text-light me-2"></i>{{$site_settings->web_site_name}}</a>, Tous droits réservés.</span>
            </div>

        </div>
    </div>
</div>
<!-- Copyright End -->


<!-- Back to Top -->
<a href="#" class="btn btn-primary border-3 border-primary rounded-circle back-to-top"><i
        class="fa fa-arrow-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('lib/easing/easing.min.js')}}"></script>
<script src="{{ asset('lib/waypoints/waypoints.min.js')}}"></script>
<script src="{{ asset('lib/lightbox/js/lightbox.min.js')}}"></script>
<script src="{{ asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<script>
    // Function to update the cart item count dynamically
    function updateCartItemCount() {
        var cartItemCountElement = document.getElementById('cartItemCount');
        var cartItemsCount = {{ count(session('cartItems') ?? []) }};
        cartItemCountElement.textContent = cartItemsCount;
    }

    // Call the function to update the count when the page loads
    window.addEventListener('load', function() {
        updateCartItemCount();
    });
</script>

<!-- Template Javascript -->
<script src="{{ asset('js/main.js')}}"></script>
@if(session('status'))
    <script>
        swal('{{ session('status') }}');
    </script>
@endif
</body>

</html>
