<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <link href="{{asset('img/logo.png')}}" rel="shortcut icon">
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=0"
    />
    <title>@yield('title')</title>
    {{--     Dashboard - Links --}}
    @livewireStyles
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
   @include('admin.partials.styles')
{{--     Fin Dashbord Link --}}
</head>

<body>
{{--Main wrapper --}}
<div class="main-wrapper">
{{--     Debut Header --}}
    @include('admin.partials.header')
{{--     Fin Header --}}
{{-- ------------------ --}}
{{--     Debut Sidebar --}}
    @include('admin.partials.sidebar')
{{--     Fin Sidebar  --------------------- --}}
{{--     Contenu de la page --}}
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="page-header">
                @yield('dashboard-header')
            </div>
           @yield('dashboard-content')
        </div>
    </div>
{{--     Fin Contenu de la page --}}
</div>
{{-- Scripts dashboard --}}
@include('admin.partials.scripts')
@livewireScripts
@stack('after-scripts')
{{-- Fin Script Dashboard --}}


{{--Alerte Flottant--}}
@if(session()->get('error'))
    <script>
        iziToast.error({
          title:"erreur",
          position:"topRight",
           message:'{{session('error')}}'
})
    </script>
@endif

@if(session()->get('success'))
    <script>
        iziToast.success({
            title:"succes",
            position:"topRight",
            message:'{{session('success')}}'
        })
    </script>
@endif
</body>
</html>
