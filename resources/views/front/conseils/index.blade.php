@extends('layouts.appfront')

@section('content')
    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Conseils</h1>
    </div>
    <!-- Single Page Header End -->
    <!-- Search Bar Start -->
    <div class="container-fluid py-3">
        <div class="container">
            <div class="d-flex position-relative"
                 style="position: fixed; top: 10px; left: 10px; z-index: 1000; width: 80%;">
                <form action="{{ route('conseils.search') }}" method="GET" class="d-flex w-100">
                    <input class="form-control border-2 border-secondary w-75 py-3 px-4 rounded-pill" type="text"
                           name="query" placeholder="Rechercher par titre...">
                    <button type="submit" class="btn btn-primary border-2 border-secondary rounded-pill text-white ms-2"
                            style="height: 100%;">Rechercher
                    </button>
                </form>
            </div>
        </div>
    </div>
    <!-- Search Bar End -->
    <br><br>
    <!-- Blog Start -->
    <div class="container">
        <div class="row g-4">
            @forelse ($conseils as $conseil)
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100 shadow-sm">
                        <img class="card-img-top" style="height: 300px; object-fit: cover;"
                             src="{{ asset($conseil->image) }}" alt="Conseil Image">
                        <div class="card-body">
                            <h5 class="card-title"><a
                                    href="{{ route('front.conseildetails.index', ['id' => $conseil->id]) }}">{{ $conseil->titre }}</a>
                            </h5>
                            <p class="card-text">{{ Illuminate\Support\Str::limit($conseil->contenu, 100) }}</p>
                        </div>
                        <div class="card-footer bg-transparent border-top">
                            <small class="text-muted">
                                <i class="fa fa-user text-primary me-1"></i>{{ $conseil->user->fullName() }}
                            </small>
                            <br>
                            <small class="text-muted">
                                <i class="fa fa-calendar text-primary me-1"></i>{{ \Carbon\Carbon::parse($conseil->created_at)->format('m-d H:i') }}
                            </small>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12">
                    <p class="text-center">Aucun conseil trouvé pour votre recherche.</p>
                </div>
            @endforelse
        </div>

        <div class="col-12">
            <div class="pagination d-flex justify-content-center mt-5">
                <div class="pagination">
                    {{ $conseils->links('pagination::bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
    <!-- Blog End -->
@endsection
