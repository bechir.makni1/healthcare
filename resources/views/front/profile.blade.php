@php use Illuminate\Support\Facades\Auth; @endphp
@extends('layouts.appfront')
@section('content')

    <div class="container-fluid py-5 mb-5 hero-header">
        <div class="container py-5">
            <div class="row g-5 align-items-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-header">
                            <div class="row align-items-center">
                                <div class="col-auto profile-image">
                                    <a href="#">
                                        @if(!is_null(auth()->user()->avatar))
                                            <img class="rounded-circle"
                                                 src="{{ asset(Auth::user()->avatar) }}"
                                                 alt="User Avatar" class="img-fluid rounded-circle" style="max-width: 150px;">
                                        @else
                                            <img src="{{ asset('img/default-user.png') }}" alt="Default Avatar" class="img-fluid rounded-circle" style="max-width: 150px;">
                                        @endif

                                    </a>
                                </div>
                                <div class="col ml-md-n2 profile-user-info">
                                    <h4 class="user-name mb-3">{{ Auth::user()->fullName() }}</h4>
                                    <h6 class="text-muted mt-1">{{  ucfirst(Auth::user()->type) }}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="profile-menu">
                            <ul class="nav nav-tabs nav-tabs-solid">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab"
                                                        href="#per_details_tab">A propos</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#password_tab">Mot de
                                        passe</a></li>
                            </ul>
                        </div>
                        <div class="tab-content profile-tab-cont">
                            <div class="tab-pane fade show active" id="per_details_tab">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title d-flex justify-content-between">
                                                    <span>Informations Personelles</span>
                                                    <a class="edit-link" data-toggle="modal"
                                                       href="#edit_personal_details"><i class="fa fa-edit mr-1"></i>Modifier
                                                    </a>
                                                </h5>
                                                <div class="row">
                                                    <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Nom</p>
                                                    <p class="col-sm-9">{{ auth()->user()->first_name }}</p>
                                                </div>
                                                <div class="row">
                                                    <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Prénom</p>
                                                    <p class="col-sm-9">{{ auth()->user()->last_name }}</p>
                                                </div>
                                                <div class="row">
                                                    <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Email</p>
                                                    <p class="col-sm-9">
                                                        <a href="">{{ auth()->user()->email }}</a>
                                                    </p>
                                                </div>
                                                @if ($errors->any())
                                                    <div class="alert alert-danger" role="alert">
                                                        <ul class="list-unstyled mb-0">
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="modal fade" id="edit_personal_details" aria-hidden="true"
                                             role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Personal Details</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <form action="{{ route('client.account.update') }}" method="POST"
                                                              enctype="multipart/form-data">
                                                            @csrf
                                                            @method('PATCH')
                                                            <div class="row form-row">

                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Nom</label>
                                                                        <input type="text" class="form-control"
                                                                               name="first_name"
                                                                               value="{{(auth()->user()->first_name)}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>prenom</label>
                                                                        <input type="text" class="form-control"
                                                                               name="last_name"
                                                                               value="{{(auth()->user()->last_name)}}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Email</label>
                                                                        <input type="email" class="form-control"
                                                                               name="email"
                                                                               value={{(auth()->user()->email)}}></div>
                                                                </div>

                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Photo de profile</label>
                                                                        <input type="file" name="avatar"
                                                                               class="form-control"></div>
                                                                </div>

                                                            </div>
                                                            <button type="submit" class="btn btn-primary btn-block">
                                                                Enregistrer les modifications
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="password_tab" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Modifier le mot de passe</h5>
                                        <div class="row">
                                            <div class="col-md-10 col-lg-6">
                                                <form action="{{route('password.update.account')}}" method="post">
                                                    @csrf

                                                    <div class="form-group">
                                                        <label>Ancien mot de passe</label>
                                                        <input type="password" name="current_password"
                                                               class="form-control"></div>
                                                    @error('current_password')
                                                    <p class="text-red-500 mt-2">{{ $message }}</p>
                                                    @enderror
                                                    <div class="form-group">
                                                        <label>Nouveau mot de passe</label>
                                                        <input type="password" name="password" class="form-control">
                                                    </div>
                                                    @error('password')
                                                    <p class="text-red-500 mt-2">{{$message}}</p>
                                                    @enderror
                                                    <div class="form-group">
                                                        <label>Confirmer mot de passe</label>
                                                        <input type="password" name="password_confirmation"
                                                               class="form-control"></div>
                                                    @error('password_confirmation')
                                                    <p class="text-red-500 mt-2">{{$message}}</p>
                                                    @enderror
                                                    <button class="btn btn-primary" type="submit">Enregistrer les
                                                        modifications
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
