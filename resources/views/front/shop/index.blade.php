@extends('layouts.appfront')

@section('content')

    <style>
        .card {
            height: 100%;
            display: flex;
            flex-direction: column;
        }

        .card-img-container {
            height: 200px; /* Fixed height for images */
            overflow: hidden;
        }

        .card-img-top {
            height: 100%;
            width: 100%;
            object-fit: cover; /* Ensure images cover the container */
        }

        .card-body {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .card-title {
            font-size: 1.25rem;
            margin-bottom: 0.5rem;
        }

        .card-text {
            flex-grow: 1;
        }

        .pagination a {
            padding: 0.5rem 0.75rem;
            margin: 0 0.25rem;
            border: 1px solid #ddd;
            border-radius: 5px;
            text-decoration: none;

        }
    </style>
    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Magasin</h1>
    </div>
    <!-- Single Page Header End -->

    <!-- Fruits Shop Start-->
    <div class="container-fluid fruite py-5">
        <div class="container py-5">
            <h1 class="mb-4">Fresh fruits shop</h1>
            <div class="row g-4">
                <div class="col-lg-12">
                    <div class="col-xl-3">
                        <div class="input-group w-100 mx-auto d-flex">
                            <form action="{{ route('shop.search') }}" method="GET" class="d-flex w-100">
                                <input type="search" class="form-control p-3" name="query" placeholder="Rechercher" aria-describedby="search-icon-1">
                                <button type="submit" class="input-group-text p-2" id="search-icon-1"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="row g-4">
                        <div class="col-lg-3">
                            <div class="row g-4">
                                <div class="col-lg-12">
                                </div>
                                <div class="col-lg-12">
                                    <div class="position-relative">
                                        <img src="img/banner-fruits.jpg" class="img-fluid w-100 rounded" alt="">
                                        <div class="position-absolute" style="top: 50%; right: 10px; transform: translateY(-50%);">
                                            <h3 class="text-secondary fw-bold">Fresh <br> Fruits <br> Banner</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row g-4 justify-content-center">
                                @foreach($produits as $key=> $produit)
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <div class="card h-100 border border-secondary">
                                            <div class="card-img-container">
                                                <img src="{{ asset($produit->image) }}" class="card-img-top" alt="{{ $produit->name }}">
                                            </div>
                                            <div class="card-body">
                                                <h4 class="card-title">{{ $produit->name }}</h4>
                                                <p class="card-text">{{ Illuminate\Support\Str::limit($produit->details, 100) }}</p>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p class="text-dark fs-5 fw-bold mb-0">{{ $produit->price }} DT</p>
                                                    <a href="{{ route('shopdetails.index', $produit->id) }}" class="btn btn-outline-primary rounded-pill px-3">Voir Plus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    <div class="col-12">
                                    <div class="pagination d-flex justify-content-center mt-5">
                                        <div class="pagination">
                                            {{ $produits->links('pagination::bootstrap-4') }}
                                        </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fruits Shop End-->
@endsection

