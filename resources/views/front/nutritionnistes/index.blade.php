@extends('layouts.appfront')
@section('content')
    <!-- Modal -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

    <style>
        .icon {
            margin-right: 7px;
        }

        p {
            margin-top: 0;
            margin-bottom: 1rem;
        }

        .fs-18 {
            font-size: 18px;
        }

        .pistache-green {
            color: #81c408; /* Couleur vert pistache */
        }
    </style>
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Nutritionnistes</h1>
        <ol class="breadcrumb justify-content-center mb-0">
        </ol>
    </div>
    <div class="container-fluid hero-header" style="margin-top: 100px">
        <div class="container  ">
            <div class="row  align-items-center">
                <br>
                <div class="col-12 ">
                    <h1>Liste des Nutritionnistes</h1>
                    <!-- Search Bar Start -->
                    <div class="container-fluid py-3">
                        <div class="container">
                            <div class="d-flex position-relative"
                                 style="position: fixed; top: 10px; left: 10px; z-index: 1000; width: 80%;">
                                <form action="{{ route('nutritionnistes.search') }}" method="GET" class="d-flex w-100">
                                    <input class="form-control border-2 border-secondary w-75 py-3 px-4 rounded-pill"
                                           type="text" name="query" placeholder="Rechercher un nutritionniste...">
                                    <button type="submit"
                                            class="btn btn-primary border-2 border-secondary rounded-pill text-white ms-2"
                                            style="height: 100%;">Rechercher
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Search Bar End -->
                    @foreach($nutritionnistes as $nutritionniste)
                        <div class="card border-0 p-4 regular mb-4 single-lawyer">
                            <article id="post-25075">
                                <div class="row pe-md-3">
                                    <div class="col-5 col-md-3">
                                        <div class="row timerate">
                                            <div class="col-12 text-center">
                                                <div class="listing-thumb position-relative">
                                                    <img loading="lazy" class="rounded-circle"
                                                         src="{{asset($nutritionniste->avatar ?? asset('img/default-user.png'))}}"
                                                         alt="image " style="width: 200px;">
                                                    <div class="online-abtest ab-test-new">
                                                        <i class="fa fa-phone  me-2 text-secondary"></i>
                                                        <span class="listing-badge online">{{ $nutritionniste->phone }}</span>
                                                        <span class="listing-badge online"></span>
                                                    </div>
                                                </div>

                                                <div class="col-12 text-center timer-holder mt-2 mt-md-4">
                                                    <div class="timerrate">
                                                        <img
                                                            src="https://www.justifit.fr/wp-content/themes/ddfr/img/timer.svg"
                                                            alt="Fast Response" class="mb-1">
                                                        <p class="mb-3">
                                                            <span> {{ $nutritionniste->gestionInformations->horaires ?? null }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7 col-md-9 px-0 mb-3 mb-md-0 align-items-center d-flex d-md-block">
                                        <div class="lawyer-content">
                                            <h2 itemprop="name"
                                                class="pb-2 mt-0 mt-md-3 pistache-green d-flex align-items-center">
                                                <a>{{ $nutritionniste->fullName() }}</a>
                                                <h6>
                                                    @php
                                                        $ratenum = number_format($nutritionniste->average_rating);
                                                    @endphp
                                                    <div class="rating">
                                                        @for($i = 1; $i <= $ratenum; $i++)
                                                            <i class="fa fa-star checked"></i>
                                                        @endfor
                                                        @for($j = $ratenum + 1; $j <= 5; $j++)
                                                            <i class="fa fa-star"></i>
                                                        @endfor
                                                        <span>
                                                            @if($nutritionniste->ratings->count() == 0)
                                                                Non Evaluation
                                                            @else
                                                                ({{ $nutritionniste->ratings->count() }})
                                                            @endif
                                                        </span>
                                                    </div>
                                                </h6>
                                            </h2>
                                            <div class="row">
                                                <div class="col-12 col-md-6 type-box mb-2">
                                                    <div class="d-flex align-items-center">
                                                        <i class="icon fa-solid fa-location-dot me-2 text-secondary"></i>
                                                        <span>{{ $nutritionniste->address }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 type-box mb-2">
                                                    <div class="d-flex align-items-center">
                                                        <i class='icon fas fa-graduation-cap me-2 text-secondary'></i>
                                                        <span>{{ $nutritionniste->gestionInformations->experience ?? null}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 type-box mb-2">
                                                    <div class="d-flex align-items-center">
                                                        <i class="fas fa-envelope me-2 text-secondary"></i>
                                                        <span>{{ $nutritionniste->email }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-2 mt-md-1 px-0">
                                            <div class="spec-desc">
                                                <h3 class="mb-2 d-flex align-items-center">
                                                    <i class="icon-law-area fs-18"></i>Spécialité
                                                </h3>
                                                <div class="row spec-box">
                                                    <div class="col-6 spec-box-item mb-1" itemprop="knowsAbout">
                                                        <span>&nbsp;&nbsp;&nbsp;{{ $nutritionniste->gestionInformations ? $nutritionniste->gestionInformations->specialite : null }}</span>
                                                    </div>
                                                    <div class="col-6 spec-box-item mb-1" itemprop="knowsAbout">
                                                        <i class='icon fas fa-globe me-2 text-secondary'></i>
                                                        <span>Langue</span>
                                                        <div class="col-6 spec-box-item mb-1" itemprop="knowsAbout">
                                                            <span>&nbsp;&nbsp;&nbsp;{{ $nutritionniste->gestionInformations->langue ?? null }}</span>
                                                        </div>
                                                    </div>

                                                    <div class="col-6 spec-box-item mb-1">
                                                        <h3 class="mb-2 d-flex align-items-center">
                                                            <i class="icon-law-area fs-18"></i>Diplôme
                                                        </h3>
                                                        <div class="col-6 spec-box-item mb-1" itemprop="knowsAbout">
                                                            <span>{{ $nutritionniste->gestionInformations->diplome ?? null }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="listing-desc mt-3 pt-3">
                                                <p itemprop="description">
                                                <h3>Bio</h3> {{ $nutritionniste->gestionInformations->bio ?? null }}
                                                </p>
                                            </div>
                                            @if(\Illuminate\Support\Facades\Auth::id() != $nutritionniste->id)
                                                <div class="buttons grid-1-buttons contacts-btn online-abtest">
                                                    <span itemprop="telephone" style="display:none">+33626914158</span>
                                                    <a href="#" rel="nofollow" data-toggle="modal"
                                                       data-target="#requestAppointmentModal"
                                                       data-nutritionist-id="{{ $nutritionniste->id }}">
                                                            @if(auth()->check() && (auth()->user()->type == \App\Models\User::TYPE_SUPER_ADMIN || auth()->user()->type == \App\Models\User::TYPE_CLIENT))
                                                                <button type="button" class="btn btn-primary">
                                                                    <span>Prendre un Rendez-Vous</span>
                                                                </button>
                                                            @endif
                                                    </a>
                                                    @if(auth()->check() && (auth()->user()->type == \App\Models\User::TYPE_CLIENT))
                                                        <!-- Button trigger modal -->
                                                        <a href="#" rel="nofollow" data-toggle="modal"
                                                           data-target="#exampleModal"
                                                           data-nutritionist-id="{{ $nutritionniste->id }}">
                                                            <button type="button" class="btn btn-primary">
                                                                <span>Evaluer nutritionniste</span>
                                                            </button>
                                                        </a>
                                                    @endif
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Modale de succès -->
    <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="successModalLabel">Succès</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ session('success') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modale d'erreur -->
    <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="errorModalLabel">Erreur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ session('error') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="requestAppointmentModal" tabindex="-1" role="dialog"
         aria-labelledby="requestAppointmentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="requestAppointmentModalLabel">Demande de Rendez-Vous</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="appointmentForm" action="{{ route('appointment.store') }}" method="POST">
                        @csrf
                        <input type="hidden" id="appointmentNutritionistId" name="nutritionist_id">
                        <input type="date" name="date" required>
                        <input type="time" name="start_at" required>
                        <button type="submit" class="btn btn-primary">Envoyer la demande</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Evaluez le nutritionniste</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="ratingForm" action="{{ route('nutritionists.rate') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="rating-css">
                            <div class="star-icon">
                                <input type="radio" value="1" name="rating" id="rating1">
                                <label for="rating1" class="fa fa-star"></label>
                                <input type="radio" value="2" name="rating" id="rating2">
                                <label for="rating2" class="fa fa-star"></label>
                                <input type="radio" value="3" name="rating" id="rating3">
                                <label for="rating3" class="fa fa-star"></label>
                                <input type="radio" value="4" name="rating" id="rating4">
                                <label for="rating4" class="fa fa-star"></label>
                                <input type="radio" value="5" name="rating" id="rating5">
                                <label for="rating5" class="fa fa-star"></label>

                                <input type="hidden" id="ratingNutritionistId" name="nutritionist_id">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Noter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function () {
        @if (session('success'))
        $('#successModal').modal('show');
        @endif

        @if (session('error'))
        $('#errorModal').modal('show');
        @endif

        // Code pour gérer les clics sur les boutons des modales
        $('[data-toggle="modal"][data-target="#requestAppointmentModal"]').click(function () {
            var nutritionistId = $(this).data('nutritionist-id');
            $('#appointmentNutritionistId').val(nutritionistId);
        });

        $('[data-toggle="modal"][data-target="#exampleModal"]').click(function () {
            var nutritionistId = $(this).data('nutritionist-id');
            $('#ratingNutritionistId').val(nutritionistId);
        });
        // Assurez-vous que le script de fermeture est correct
        $('.close, .btn[data-dismiss="modal"]').click(function() {
            $('.modal').modal('hide');
        });
    });

    $(document).ready(function () {
        $('[data-toggle="modal"][data-target="#requestAppointmentModal"]').click(function () {
            var nutritionistId = $(this).data('nutritionist-id');
            $('#appointmentNutritionistId').val(nutritionistId);
        });

        $('[data-toggle="modal"][data-target="#exampleModal"]').click(function () {
            var nutritionistId = $(this).data('nutritionist-id');
            $('#ratingNutritionistId').val(nutritionistId);
        });
    });
</script>
