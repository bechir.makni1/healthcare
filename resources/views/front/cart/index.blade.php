@extends('layouts.appfront')

@section('content')
    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Panier</h1>
    </div>
    <!-- Single Page Header End -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Produits</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prix</th>
                        <th scope="col">Quantité</th>
                        <th scope="col">Total</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                    </thead>
                    @php
                        $total = 0;
                        $totalLivraison = 0; // Initialize the variable
                    @endphp
                    @if (session('cartItems'))
                        @foreach (session('cartItems') as $key => $cartItem)
                            <tbody>

                            <tr>
                                <th scope="row">
                                    <div class="d-flex align-items-center">
                                        <img src="{{$cartItem['image']  }}" class="img-fluid me-5 rounded-circle" style="width: 80px; height: 80px;" alt="{{$cartItem['name']}}">
                                    </div>
                                </th>
                                <td>
                                    <p class="mb-0 mt-4">{{$cartItem['name']}}</p>
                                </td>
                                <td>
                                    <p class="mb-0 mt-4">{{$cartItem['price']}} DT</p>
                                </td>
                                <td>
                                    <form action="{{route('modifier.cart', $key)}}" method="POST">
                                        @csrf
                                        <div class="input-group quantity mt-4" style="width: 100px;">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-minus rounded-circle bg-light border" onclick="updateQuantity(this, -1)">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                            <input type="hidden" name="id" value="{{ $key }}">
                                            <input type="text" name="quantity" class="form-control form-control-sm text-center border-0" value="{{$cartItem['quantity']}}">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-plus rounded-circle bg-light border" onclick="updateQuantity(this, 1)">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <p class="mb-0 mt-4"> {{$cartItem['quantity'] * $cartItem['price']}} DT</p>
                                </td>
                                <td class="px-6 whitespace-nowrap text-center text-sm font-medium">
                                    <a href="{{ route('supprimer.cart', $key) }}" role="button" class="btn btn-sm btn-outline-danger">Supprimer</a>
                                </td>
                            </tr>
                            </tbody>
                            @php
                                $subtotal = $cartItem['quantity'] * $cartItem['price'];
                                $total += $subtotal;
                                $totalLivraison += $cartItem['livraison']; // Aggregate livraison costs
                            @endphp
                        @endforeach
                    @else
                        <td align="left" colspan="6">
                            <p class="font-bold text-l text-black py-6 px-4">
                                Le panier est vide.
                            </p>
                        </td>
                    @endif
                </table>
            </div>
            <div class="mt-5">
                <input type="text" class="border-0 border-bottom rounded me-5 py-3 mb-4" placeholder="Code promo">
                <button class="btn border-secondary rounded-pill px-4 py-3 text-primary" type="button">Appliquer Coupon</button>
            </div>
            <div class="row g-4 justify-content-end">
                <div class="col-8"></div>
                <div class="col-sm-8 col-md-7 col-lg-6 col-xl-4">
                    <div class="bg-light rounded">
                        <div class="p-4">
                            <h1 class="display-6 mb-4">Total <span class="fw-normal">du panier</span></h1>
                            <div class="d-flex justify-content-between mb-4">
                                <h5 class="mb-0 me-4">Sous-total:</h5>
                                <p class="mb-0">{{ $total }} DT</p>
                            </div>
                            <div class="d-flex justify-content-between">
                                <h5 class="mb-0 me-4">Livraison</h5>
                                <div class="">
                                    <p class="mb-0">Tarif fixe: {{$totalLivraison}} DT</p>
                                </div>
                            </div>
                            <p class="mb-0 text-end">Livraison sur toutes la Tunisie.</p>
                        </div>
                        <div class="py-4 mb-4 border-top border-bottom d-flex justify-content-between">
                            <h5 class="mb-0 ps-4 me-4">Total</h5>
                            <p class="mb-0 pe-4">{{ $total + $totalLivraison }} DT</p>
                        </div>
                        <a href="{{route('front.caisse.index')}}" class="btn border-secondary rounded-pill px-4 py-3 text-primary text-uppercase mb-4 ms-4">Passer à la caisse</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function updateQuantity(button, delta) {
            var input = button.closest('.input-group').querySelector('input[name="quantity"]');
            var quantity = parseInt(input.value) + delta;
            if (quantity < 1) quantity = 1;
            input.value = quantity;
            button.closest('form').submit();
        }
    </script>
@endsection
