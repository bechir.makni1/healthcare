@extends('layouts.appfront')

@section('content')
    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Favoris</h1>
    </div>
    <!-- Single Page Header End -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Produit</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prix</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                    </thead>
                    @if ($wishlistItems->count() > 0)
                        @foreach ($wishlistItems as $item)
                            <tbody>
                            <tr>
                                <th scope="row">
                                    <div class="d-flex align-items-center">
                                        <img src="{{ asset($item->produits->image) }}" class="img-fluid me-5 rounded-circle" style="width: 80px; height: 80px;" alt="{{ $item->produits->name }}">
                                    </div>
                                </th>
                                <td>
                                    <p class="mb-0 mt-4">{{ $item->produits->name }}</p>
                                </td>
                                <td>
                                    <p class="mb-0 mt-4">{{ $item->produits->price }} DT</p>
                                </td>
                                <td class="mb-0 mt-4">
                                    <form action="{{ route('wishlist.remove', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-outline-danger">Supprimer</button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    @else
                        <tbody>
                        <tr>
                            <td colspan="4">
                                <div class="d-flex justify-content-center align-items-center" style="height: 200px;">
                                    <p class="font-bold text-l text-black text-center">
                                        oops! La Page Favoris est vide.
                                    </p>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
