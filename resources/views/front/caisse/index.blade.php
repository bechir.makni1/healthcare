@extends('layouts.appfront')

@section('content')
    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Caisse</h1>
    </div>
    <!-- Single Page Header End -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <h1 class="mb-4">Remplissez le formulaire pour passer la commande</h1>
            <br>
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Error</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('commande.store') }}" method="POST">
                @csrf
                <div class="row g-5">
                    <div class="col-md-12 col-lg-6 col-xl-7">
                        <div class="form-item">
                            <label class="form-label my-3">Addresse</label>
                            <input type="text" name="addresse" class="form-control" placeholder="Numéro de la maison Nom de la rue">
                            @error('address')
                            <p class="text-red-500 mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-item">
                            <label class="form-label my-3">Code Postal</label>
                            <input type="text" name="code_postal" class="form-control">
                            @error('postal_code')
                            <p class="text-red-500 mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-item">
                            <label class="form-label my-3">Pays</label>
                            <input type="text" name="pays" class="form-control">
                            @error('country')
                            <p class="text-red-500 mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-item">
                            <label class="form-label my-3">Ville</label>
                            <input type="text" name="ville" class="form-control">
                            @error('city')
                            <p class="text-red-500 mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-item">
                            <label class="form-label my-3">Téléphone</label>
                            <input type="text" name="telephone" class="form-control">
                            @error('telephone')
                            <p class="text-red-500 mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <hr>
                        <div class="form-item">
                            <textarea name="note" class="form-control" spellcheck="false" cols="30" rows="11" placeholder="Notes de commande (optionnelles)"></textarea>
                            @error('note')
                            <p class="text-red-500 mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-5">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Produits</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Prix</th>
                                    <th scope="col">Quantité</th>
                                    <th scope="col">Livraison</th>
                                    <th scope="col">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($cartItems as $key => $cartItem)
                                    <tr>
                                        <th scope="row">
                                            <div class="d-flex align-items-center mt-2">
                                                <img src="{{asset ($cartItem['image']) }}" class="img-fluid me-5 rounded-circle" style="width: 80px; height: 80px;" alt="{{$cartItem['name']}}">
                                                <input type="hidden" name="product_ids[]" value="{{ $key }}">
                                                <input type="hidden" name="quantities[]" value="{{ $cartItem['quantity'] }}">
                                            </div>
                                        </th>
                                        <td class="py-5">{{ $cartItem['name'] }}</td>
                                        <td class="py-5">{{ $cartItem['price'] }}</td>
                                        <td class="py-5">{{ $cartItem['quantity'] }}</td>
                                        <td class="py-5">{{ $cartItem['livraison'] }}</td>
                                        <td class="py-5">{{ $cartItem['quantity'] * $cartItem['price'] }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th scope="row"></th>
                                    <td class="py-5"></td>
                                    <td class="py-5"></td>
                                    <td class="py-5">
                                        <p class="mb-0 text-dark py-3">Subtotal</p>
                                    </td>
                                    <td class="py-5">
                                        <div class="py-3 border-bottom border-top">
                                            <p class="mb-0 text-dark">{{ $subtotal }} DT</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"></th>
                                    <td class="py-5">
                                        <p class="mb-0 text-dark text-uppercase py-3">TOTAL</p>
                                    </td>
                                    <td class="py-5"></td>
                                    <td class="py-5"></td>
                                    <td class="py-5">
                                        <div class="py-3 border-bottom border-top">
                                            <p class="mb-0 text-dark">{{ $total }} DT</p>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row g-4 text-center align-items-center justify-content-center pt-4">
                            <button type="submit" class="btn border-secondary py-3 px-4 text-uppercase w-100 text-primary">Passer la commande</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
