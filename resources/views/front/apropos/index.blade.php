@extends('layouts.appfront')
@section('content')
    <!-- Hero Start -->
    <div class="container-fluid py-5 mb-5 hero-header">
        <div class="container py-5">
            <div class="row g-5 align-items-center">
                <div class="col-md-12 col-lg-7">
                    <h4 class="mb-3 text-secondary">Centre des Délices Sains</h4>
                    <h1 class="mb-5 display-3 text-primary">À propos de nous</h1>
                    <p class="text-dark">
{{--                        Bienvenue sur HealthCare, votre destination incontournable pour une alimentation saine et équilibrée.--}}
{{--                        Notre plateforme offre une vaste sélection de recettes délicieuses et nutritives,--}}
{{--                        toutes élaborées en collaboration avec des nutritionnistes qualifiés pour vous garantir des choix alimentaires--}}
{{--                        aussi savoureux que bénéfiques pour votre santé.--}}
{{--                        En plus de nos recettes, nous fournissons des conseils avisés--}}
{{--                        sur les aliments les plus sains disponibles dans les supermarchés, ainsi que des réponses claires et précises aux--}}
{{--                        questions importantes sur l'alimentation et la santé. Nous comprenons l'importance d'un accompagnement professionnel--}}
{{--                        dans votre quête d'une meilleure santé, c'est pourquoi nous facilitons la recherche et la prise de rendez-vous avec des--}}
{{--                        nutritionnistes directement depuis notre plateforme. De plus, notre boutique en ligne vous permet de commander des produits--}}
{{--                        soigneusement sélectionnés pour compléter votre mode de vie sain. Chez HealthyFood.com, notre mission est de vous fournir--}}
{{--                        les outils, les conseils et les ressources nécessaires pour atteindre vos objectifs de bien-être et de vitalité.--}}
{{--                    </p>--}}
                    <p class="mb-4"> {{$site_settings->about}}</p>
                </div>
                <div class="col-md-12 col-lg-5">
                    <div id="carouselId" class="carousel slide position-relative" data-bs-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active rounded">
                                <img src="img/apropos.jpg" class="img-fluid w-100 h-100 bg-secondary rounded" alt="First slide">
                                <a href="#" class="btn px-4 py-2 text-white rounded">Qui Somme Nous! </a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
