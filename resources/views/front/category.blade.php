@extends('layouts.appfront')

@section('content')

    <style>
        .fruite-item {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            height: 100%;
        }

        .fruite-img {
            height: 200px;
            overflow: hidden;
        }

        .fruite-content {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .fruite-content h4 {
            min-height: 3em; /* Adjust this value as needed */
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
        }

        .fruite-content p {
            flex-grow: 1;
        }

    </style>

    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Les Recettes</h1>
    </div>
    <!-- Single Page Header End -->

    <!-- Search Bar Start -->
    <div class="container-fluid py-3">
        <div class="container">
            <div class="d-flex position-relative" style="position: fixed; top: 10px; left: 10px; z-index: 1000; width: 80%;">
                <form action="{{ route('recette.search') }}" method="GET" class="d-flex w-100">
                    <input class="form-control border-2 border-secondary w-75 py-3 px-4 rounded-pill" type="text" name="query" placeholder="Rechercher par titre...">
                    <button type="submit" class="btn btn-primary border-2 border-secondary rounded-pill text-white ms-2" style="height: 100%;">Rechercher</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Search Bar End -->

    <!-- Fruits Shop Start-->
    <div class="container-fluid fruite py-5" style="padding-top: 100px;"> <!-- Adjust padding top to avoid overlap -->
        <div class="container">
            <div class="row g-4">
                <div class="col-lg-9">
                    <div class="row g-4 justify-content-center">
                        @foreach ($category->recettes as $recette)
                            <div class="col-md-6 col-lg-6 col-xl-4">
                                <div class="rounded position-relative fruite-item">
                                    <div class="fruite-img">
                                        <img src="{{ asset($recette->image) }}" class="img-fluid w-100 rounded-top" alt="" style="height: 200px; object-fit: cover;">
                                    </div>
                                    <div class="p-4 border border-secondary border-top-0 rounded-bottom fruite-content">
                                        <h4>{{ $recette->title }}</h4>
                                        <p>{{ Illuminate\Support\Str::limit($recette->ingrédient, 100) }}</p>
                                        <div class="d-flex justify-content-between flex-lg-wrap">
                                            <p class="text-dark fs-5 fw-bold mb-0"></p>
                                            <a href="{{ route('front.recettedetails.index', $recette->id) }}" class="btn border border-secondary rounded-pill px-3 text-primary">Voir Plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="col-12">
                            <div class="pagination d-flex justify-content-center mt-5">
                                <a href="#" class="rounded">«</a>
                                <a href="#" class="active rounded">1</a>
                                <a href="#" class="rounded">2</a>
                                <a href="#" class="rounded">3</a>
                                <a href="#" class="rounded">4</a>
                                <a href="#" class="rounded">5</a>
                                <a href="#" class="rounded">6</a>
                                <a href="#" class="rounded">»</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fruits Shop End -->
@endsection
