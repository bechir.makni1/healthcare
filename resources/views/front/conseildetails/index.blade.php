@extends('layouts.appfront') <!-- Vous pouvez ajuster le layout selon votre configuration -->

@section('content')
    <style>
        .align-justify {
            text-align: justify;
        }
    </style>
    <!-- Single Page Header start -->
    <div class="container-fluid page-header py-5">
        <h1 class="text-center text-white display-6">Conseil Détail</h1>
    </div>
    <!-- Single Page Header End -->

    <!-- Single Product Start -->
    <div class="container-fluid py-5 mt-5">
        <div class="container py-5">
            <div class="row g-4">
                <div class="col-lg-6">
                    <div class="border rounded">
                        <a href="#">
                            <img src="{{ asset($conseils->image) }}" class="img-fluid rounded" alt="Image" style="height: 100%; width: 100%;">
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 align-justify">
                    <h4 class="fw-bold mb-3">{{ $conseils->titre }}</h4>
                    <p class="mb-4">{{ $conseils->contenu }}</p>
                </div>
                <div class="col-lg-12">
                    <nav>
                        <div class="nav nav-tabs mb-3">
                            <button class="nav-link border-white border-bottom-0 active" type="button" role="tab"
                                    id="nav-mission-tab" data-bs-toggle="tab" data-bs-target="#nav-mission"
                                    aria-controls="nav-mission" aria-selected="true">Avis
                            </button>
                        </div>
                    </nav>
                    <div class="tab-content mb-5">
                        <div class="tab-pane fade show active" id="nav-mission" role="tabpanel"
                             aria-labelledby="nav-mission-tab">
                            @foreach ($comments as $comment)
                                <div class="d-flex mb-3">
                                    <img src="{{ asset($comment->user->avatar ? $comment->user->avatar : 'admin/assets/img/default-user.png') }}" class="img-fluid rounded-circle p-3" style="width: 100px; height: 100px;" alt="">
                                    <div class="ms-3">
                                        <h5 class="fw-bold">{{ $comment->user->fullName() }}</h5>
                                        <p class="text-muted" style="font-size: 12px;">{{ \Carbon\Carbon::parse($comment->created_at)->format('m-d  H:i') }}</p>
                                        <p>{{ $comment->content }}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <form action="{{ route('comment.create.conseil', $conseils->id) }}" id="commentform" class="comment-form" method="POST" novalidate>
                        @csrf
                        @method('POST')
                        <h4 class="mb-5 fw-bold">Laisser un commentaire</h4>
                        <div class="row g-4">
                            <div class="col-lg-12">
                                <div class="border-bottom rounded my-4">
                                    <textarea name="content" id="content" class="form-control border-0" cols="30" rows="6" placeholder="Votre commentaire*" required></textarea>
                                </div>
                            </div>
                            @if ($errors->any())
                                <div class="col-lg-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <div class="col-lg-12">
                                <div class="d-flex justify-content-between py-3 mb-5">
                                    @auth
                                        <button type="submit" class="btn border border-secondary text-primary rounded-pill px-4 py-3">Post Comment</button>
                                    @else
                                        <a href="{{ route('login') }}" class="btn border border-secondary text-primary rounded-pill px-4 py-3">Post Comment</a>
                                    @endauth
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Single Product End -->
@endsection
