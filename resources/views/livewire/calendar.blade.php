
<div>
    <div id="calendar"></div>
</div>

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                // Options and callbacks here
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: @json($events),
                selectable: true,
                selectHelper: true,
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                eventRender: function(event, element) {
                    // Customize event rendering
                    element.find('.fc-title').append('<i class="fas fa-check"></i>'); // Add check icon for confirmed events
                }
            });
        });
    </script>
@endpush
