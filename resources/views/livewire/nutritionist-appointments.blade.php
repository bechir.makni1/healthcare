<div>
    <h2>Les rendez-vous en attente</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Date</th>
            <th>Heure</th>
            <th>Client</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($appointments as $appointment)
            <tr>
                <td>{{ $appointment->date }}</td>
                <td>{{ $appointment->start_at }}</td>
                <td>{{ $appointment->user->fullName() }}</td>
                <td>
                    <button wire:click="acceptAppointment({{ $appointment->id }})" class="btn btn-success">Accepter</button>
                    <button wire:click="refuserAppointment({{ $appointment->id }})" class="btn btn-danger">Refuser</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
