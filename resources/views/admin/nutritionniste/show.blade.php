@extends('layouts.admin.app')
@section('title','création - nutritionniste')
@section('dashboard-header')

    <div class="page-header mt-5">
        <div class="row">
            <div class="col">
                <h3 class="page-title">Profile</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('nutritionniste.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('dashboard-content')

    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="profile-header">
                    <div class="row align-items-center">
                        <div class="col-auto profile-image">
                            <img class="rounded-circle" alt="User Image"
                                 src="{{ asset($nutritionniste->avatar) }}" alt="Avatar du nutritionniste"
{{--                                 src="{{ $nutritionniste->avatar ? $nutritionniste->avatar : asset('admin/assets/img/default-user.png') }} "--}}
                                 style="height: 50px;width: 50px" alt="User Image">
                        </div>
                        <div class="col ml-md-n2 profile-user-info">
                            <h4 class="user-name mb-3">
                                {{$nutritionniste->fullName()}}
                                @if($nutritionniste->isApproved())
                                    <i class="fas fa-check-circle text-success"></i>
                                @endif
                            </h4>

                            <h6 class="text-muted mt-1">{{ ucfirst($nutritionniste->type)}}</h6>
                        </div>
                        @if($nutritionniste->approved === 0)
                            <form action="{{ route('nutritionnistes.approve', $nutritionniste) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="btn btn-success">Approuver</button>
                            </form>
                        @endif
                    </div>
                </div>
                <div class="profile-menu">
                    <ul class="nav nav-tabs nav-tabs-solid">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#per_details_tab">A propos</a>
                        </li>
                        <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#password_tab">Mot de
                                passe</a></li>
                        <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#document">Document(s)</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content profile-tab-cont">
                    <div class="tab-pane fade" id="per_details_tab">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title d-flex justify-content-between">
                                            <span>Informations Personelles</span>
                                            <a class="edit-link" data-toggle="modal" href="#edit_personal_details"><i
                                                    class="fa fa-edit mr-1"></i>Modifier
                                            </a>
                                        </h5>
                                        <div class="row mt-5">
                                            <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Prenom</p>
                                            <p class="col-sm-9">{{$nutritionniste->first_name}}</p>
                                        </div>
                                        <div class="row">
                                            <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Nom</p>
                                            <p class="col-sm-9">{{$nutritionniste->last_name}}</p>
                                        </div>
                                        <div class="row">
                                            <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Email</p>
                                            <p class="col-sm-9">
                                                <a href="">{{$nutritionniste->email}}</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="edit_personal_details" aria-hidden="true" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Personal Details</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="row form-row">
                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label>Prenom</label>
                                                                <input type="text" class="form-control"
                                                                       value="{{$nutritionniste->first_name}}"></div>
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label>Nom</label>
                                                                <input type="text" class="form-control"
                                                                       value="{{$nutritionniste->last_name}}"></div>
                                                        </div>

                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label>Email</label> <input type="email"
                                                                                            class="form-control"
                                                                                            value="{{$nutritionniste->email}}">
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-block">Enregistrer
                                                        les modifications
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="password_tab" class="tab-pane fade active show">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Modifier le mot de passe</h5>
                                <div class="row">
                                    <div class="col-md-10 col-lg-6">
                                        <form>
                                            <div class="form-group">
                                                <label>Ancien mot de passe</label>
                                                <input type="password" class="form-control"></div>
                                            <div class="form-group">
                                                <label>Nouveau mot de passe</label>
                                                <input type="password" class="form-control"></div>
                                            <div class="form-group">
                                                <label>Confirmer motde passe</label>
                                                <input type="password" class="form-control"></div>
                                            <button class="btn btn-primary" type="submit">Enregistrer les
                                                modifications
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="document" class="tab-pane fade active show">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Documents</h5>
                                <br>
                                @foreach($nutritionniste->attachments as $attachment)
                                    <a href="{{ asset('uploads/' . $attachment->document) }}"
                                       target="_blank">{{ $attachment->document }}</a><br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

