@extends('layouts.admin.app')
@section('title','création - nutritionniste')
@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">
                Ajouter un nutritionniste
            </h3>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('nutritionniste.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="type" value="{{ \App\Models\User::TYPE_NUTRITIONNISTE }}">

                <!-- Avatar -->
                <div class="form-group">
                    <label for="file-input" style="text-align: -webkit-center;">
                        <img id="previewImg" src="{{ asset('admin/assets/img/default-user.png') }}"
                             style="width: 150px; height: 150px;border-radius: 100%;"/>
                    </label>
                    <input id="file-input" type="file" onchange="previewFile(this);" style="display: none;"
                           name="avatar" class="form-control" placeholder="photo">
                    @error('avatar')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="row formtype">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name">Nom</label>
                            <input
                                id="first_name"
                                class="form-control"
                                type="text"
                                name="first_name"
                                required
                            />
                            @error('first_name')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="last_name">Prénom</label>
                            <input
                                id="last_name"
                                class="form-control"
                                type="text"
                                name="last_name"
                                required
                            />
                            @error('last_name')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input
                                id="email"
                                class="form-control"
                                type="email"
                                name="email"
                                required
                            />
                            @error('email')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input
                                id="password"
                                class="form-control"
                                type="password"
                                name="password"
                                required
                            />
                            @error('password')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="address">Adresse</label>
                            <input
                                id="address"
                                class="form-control"
                                type="text"
                                name="address"
                                required
                            />
                            @error('address')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Téléphone</label>
                            <input
                                id="phone"
                                class="form-control"
                                type="text"
                                name="phone"
                            />
                            @error('phone')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="isActive">Activation</label>
                            <select class="form-control" id="isActive" name="isActive">
                                <option value="1">Activer</option>
                                <option value="0">Ne pas activer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary buttonedit1">
                    Enregistrer
                </button>
            </form>
        </div>
    </div>

@endsection

