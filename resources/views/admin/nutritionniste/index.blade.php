@extends('layouts.admin.app')
@section('title', 'Dashboard - Nutritionniste')
@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Nutritionniste</h4>
                    <a href="{{ route('nutritionniste.create') }}" class="btn btn-primary float-right veiwbutton">
                        Ajouter une nutritionniste</a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body booking_card">
                    <div class="table-responsive">
                        <table class="datatable table table-stripped table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Email</th>
                                <th>Adresse</th>
                                <th>Telephone</th>
                                <th>Approvée</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nutritionnistes as $nutritionniste)
                                <tr>
                                    <td>
                                        @if (!empty($nutritionniste->avatar))
                                            <img class="rounded-circle" src="{{ asset($nutritionniste->avatar) }}"
                                                 alt="User Avatar" style="height: 50px;width: 50px">
                                        @else
                                            <img class="rounded-circle"
                                                 src="{{ asset('admin/assets/img/default-user.png') }}"
                                                 alt="Default User Avatar" style="height: 50px;width: 50px">
                                        @endif
                                    </td>
                                    <td>{{ $nutritionniste->first_name }}</td>
                                    <td>{{ $nutritionniste->last_name }}</td>
                                    <td>{{ $nutritionniste->email }}</td>
                                    <td>{{ $nutritionniste->address ?? 'N/A' }}</td>
                                    <td>{{ $nutritionniste->phone }}</td>
                                    <td class="text-center">
                                            <span class="badge badge-pill bg-success inv-badge">
                                               {{ $nutritionniste->approved == 1 ? "Approved" : "Not Approved" }}
                                            </span>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown dropdown-action">
                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <i class="fas fa-ellipsis-v ellipse_color"></i>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item"
                                                   href="{{ route('nutritionniste.show', $nutritionniste->id) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> voir
                                                </a>
                                                <a class="dropdown-item"
                                                   href="{{ route('nutritionniste.edit', $nutritionniste->id) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> Modifier
                                                </a>
                                                <a class="dropdown-item" href="#" data-toggle="modal"
                                                   data-target="#delete_category_{{ $nutritionniste->id }}">
                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="fas fa-trash-alt m-r-5"></i> Supprimer
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div id="delete_category_{{ $nutritionniste->id }}" class="modal fade delete-modal"
                                     role="dialog">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-body text-center">
                                                <img src=" {{asset('admin/assets/img/sent.png')}}" alt="" width="50"
                                                     height="46"/>
                                                <h3 class="delete_class">
                                                    Êtes-vous sûr de vouloir supprimer ce nutritionniste ?
                                                </h3>
                                                <div class="m-t-20">
                                                    <form
                                                        action="{{ route('nutritionniste.destroy', $nutritionniste) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                                        <button type="button" class="btn btn-white"
                                                                data-dismiss="modal">Fermer
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
