@extends('layouts.admin.app')

@section('title', 'Modification de nutritionniste')

@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Modifier un nutritionniste</h4>
                    <a href="{{ route('nutritionniste.index') }}"
                       class="btn btn-primary float-right veiwbutton">Retour</a>
                </div>
            </div>
        </div>
    </div>
@endsection
<style>
    .avatar-container {
        position: relative;
        display: inline-block;
    }

    .avatar-container img {
        max-width: 100px;
        max-height: 100px;
        border-radius: 50%;
        cursor: pointer;
    }

    #avatarLabel {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: 50%;
        cursor: pointer;
    }

</style>
@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body">
                    <form action="{{ route('nutritionniste.update',['nutritionniste' => $nutritionniste]) }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Image</label>
                            <input type="file" class="form-control @error('avatar') is-invalid @enderror" id="avatar"
                                   name="avatar" value="{{ old('avatar', $nutritionniste->avatar) }}" required>
                            @error('avatar')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror"
                                   id="first_name"
                                   name="first_name" value="{{ old('first_name', $nutritionniste->first_name) }}"
                                   required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">prenom</label>
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror"
                                   id="last_name"
                                   name="last_name" value="{{ old('last_name', $nutritionniste->last_name) }}" required>
                            @error('prenom')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">email </label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                                   name="email" value="{{ old('email', $nutritionniste->email) }}" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">password </label>
                            <input type="text" class="form-control @error('password') is-invalid @enderror"
                                   id="password" name="password"
                                   value="{{ old('password', $nutritionniste->password) }}" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">adresse </label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" id="address"
                                   name="address" value="{{ old('address', $nutritionniste->address) }}" required>
                            @error('adresse')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">telephone </label>
                            <input type="text" class="form-control @error('telephone') is-invalid @enderror"
                                   id="telephone" name="phone"
                                   value="{{ old('phone', $nutritionniste->phone) }}" required>
                            @error('telephone')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="isActive">Statut</label>
                            <select class="form-control @error('isActive') is-invalid @enderror" id="isActive"
                                    name="isActive">
                                <option
                                    value="1" {{ old('isActive', $nutritionniste->isActive) == 1 ? 'selected' : '' }}>
                                    Activer
                                </option>
                                <option
                                    value="0" {{ old('isActive', $nutritionniste->isActive) == 0 ? 'selected' : '' }}>
                                    Désactiver
                                </option>
                            </select>
                            @error('isActive')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
