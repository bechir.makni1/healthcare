@extends('layouts.admin.app')
@section('title', "Page details commande")
@section('dashboard-header')
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Details de commande</h4>
        </div>
        @endsection
        @section('dashboard-content')
            @foreach($commande->products as $product)
                <div class="row mt-3">
                    <div class="col-md-8">
                        <div class="blog-view">
                            <article class="blog blog-single-post">
                                <h3 class="blog-title">{{$product->name}}</h3>
                                <div class="blog-image">
                                    <a href="blog-details.html"><img alt="" src="{{$product->imageUrl()}}"
                                                                     class="img-fluid mt-4"></a>
                                </div>
                                <div class="blog-content mt-4">
                                    <h4><b>Note </b></h4>
                                    <p>{{$commande->note}}</p>
                                </div>
                                <div class="blog-content mt-4">
                                    <h4><b>Telephone </b></h4>
                                    <p>{{$commande->telephone}}</p>
                                </div>
                                <div class="blog-content mt-4">
                                    <h4><b>Code_postal </b></h4>
                                    <p>{{$commande->code_postal}}</p>
                                </div>
                                <div class="blog-content mt-4">
                                    <h4><b>Pays </b></h4>
                                    <p>{{$commande->pays}}</p>
                                </div>
                                <div class="blog-content mt-4">
                                    <h4><b>Ville </b></h4>
                                    <p>{{$commande->ville}}</p>
                                </div>
                                <div class="blog-content mt-4">
                                    <h4><b>Adresse </b></h4>
                                    <p>{{$commande->addresse}}</p>
                                </div>
                            </article>

                            <div class="widget author-widget clearfix">
                                <h3>A propos de Client</h3>
                                <div class="about-author">
                                    <div class="about-author-img">
                                        <div class="author-img-wrap">
                                            <img class="img-fluid" alt="" src="{{ asset($commande->user->avatar) }}">
                                        </div>
                                    </div>
                                    <div class="author-details"><span
                                            class="blog-author-name">{{$commande->user->fullName()}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
@endsection
