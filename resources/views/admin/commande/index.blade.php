@extends('layouts.admin.app')
@section('title', 'Liste des commandes')

@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <div class="mt-5">
                <h4 class="card-title float-left mt-2">Commandes</h4>
            </div>
        </div>
    </div>
@endsection

@section('dashboard-content')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-table">
                            <div class="card-body booking_card">
                                <div class="table-responsive">
                                    <table class="datatable table table-stripped table-hover table-center mb-0">
                                        <thead>
                                        <tr>
                                            <th>ID Commande</th>
                                            <th>Client</th>
                                            <th>Produits</th>
                                            <th>Quantité</th>
                                            <th>Total</th>
                                            <th>Date</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($commandes as $commande)
                                            <tr>
                                                <td>{{ $commande->id }}</td>
                                                <td>
                                                    <h2 class="table-avatar">
                                                        <a href="#" class="avatar avatar-sm mr-2">
                                                            <img class="avatar-img rounded-circle" src="{{ asset($commande->user->avatar) }}" alt="{{ $commande->user->fullName() }}">
                                                        </a>
                                                        <a href="#">{{ $commande->user->fullName() }}</a>
                                                    </h2>
                                                </td>
                                                <td>
                                                    @foreach($commande->products as $product)
                                                        <p>{{ $product->name }}</p>
                                                    @endforeach
                                                </td><td>
                                                    @foreach($commande->products as $product)
                                                        <p> {{ $product->pivot->quantity }}</p>
                                                    @endforeach
                                                </td>
                                                <td>{{ $commande->total }} DT</td>
                                                <td>{{ $commande->created_at->format('d-m-Y') }}</td>
                                                <td class="text-right">
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="fas fa-ellipsis-v ellipse_color"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('commande.voir', $commande->id) }}"><i class="fas fa-eye m-r-5"></i> Voir</a>
                                                            <a class="dropdown-item" href="{{route('admin.commandes.confirm',$commande->id)}}"><i class="fa-check-alt m-r-5"></i> Confirmer</a>
                                                            <a class="dropdown-item" href="{{route('admin.commandes.annuler',$commande->id)}}"><i class="fa-check-alt m-r-5"></i> Annuler</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <!-- Delete Modal -->

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
