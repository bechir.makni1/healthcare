@extends('layouts.admin.app')
@section('title', "page d acceuil")
@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{route('settings.update',$settings)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <h3 class="page-title">Paramètres de site</h3>
                <div class="row mt-4">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nom du site <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="web_site_name"
                                   value="{{ isset($settings) ? old('web_site_name', $settings->web_site_name) : old('web_site_name') }}"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="logo">
                                <label class="custom-file-label" for="customFile">Choisir une image</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Address</label>
                            <input class="form-control"
                                   value="{{ isset($settings) ? old('address', $settings->address) : old('address') }}"
                                   type="text" name="address"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Numero de telephone</label>
                            <input class="form-control"
                                   value="{{ isset($settings) ? old('phone', $settings->phone) : old('phone') }}"
                                   name="phone" type="text"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control"
                                   value="{{ isset($settings) ? old('email', $settings->email) : old('email') }}"
                                   name="email" type="text"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="5" id="comment"
                                      name="about">{{ isset($settings) ? old('about', $settings->about) : old('about') }}</textarea>
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary buttonedit mr-5 mt-5">Enregister Les Modifications</button>
            </form>
        </div>
    </div>
@endsection
