@php use Illuminate\Support\Facades\Auth; @endphp
@extends('layouts.admin.app')

@section('title', 'dashboard-profile')

@section('dashboard-header')
    <div class="page-header mt-5">
        <div class="row">
            <div class="col">
                <h3 class="page-title">Profile</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-header">
                <div class="row align-items-center">
                    <div class="col-auto profile-image">
                        <a href="#">
                            @if(is_null(auth()->user()->avatar))
                                <img class="rounded-circle" alt="User Image" src="{{ asset('admin/assets/img/profiles/avatar-01.png') }}">
                            @else
                                <img class="rounded-circle" alt="User Image" src="{{ asset(auth()->user()->avatar) }}">
                            @endif
                        </a>
                    </div>
                    <div class="col ml-md-n2 profile-user-info">
                        <h4 class="user-name mb-3">{{ Auth::user()->first_name }}</h4>
                        <h4 class="user-name mb-3">{{ Auth::user()->last_name }}</h4>
                        <h6 class="text-muted mt-1">{{ ucfirst(Auth::user()->type) }}</h6>
                    </div>
                </div>
            </div>
            <div class="profile-menu">
                <ul class="nav nav-tabs nav-tabs-solid">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#per_details_tab">A propos</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#password_tab">Mot de passe</a></li>
                    @if(Auth::check() && !Auth::user()->gestionInformations && Auth::user()->type != \App\Models\User::TYPE_SUPER_ADMIN)
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#information_tab">Informations Personelles</a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="tab-content profile-tab-cont">
                <div class="tab-pane fade show active" id="per_details_tab">
                    <div class="row">
                        <div class="col-lg-12">
                            @if(Auth::user()->type === 'super admin' || is_null($compte))
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title d-flex justify-content-between">
                                            <span>Informations Personelles</span>
                                            <a class="edit-link" data-toggle="modal" href="#edit_personal_details"><i class="fa fa-edit mr-1"></i>Modifier</a>
                                        </h5>
                                        <div class="row">
                                            <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Nom</p>
                                            <p class="col-sm-9">{{ Auth::user()->first_name }}</p>
                                        </div>
                                        <div class="row">
                                            <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Prénon</p>
                                            <p class="col-sm-9">{{ Auth::user()->last_name }}</p>
                                        </div>
                                        <div class="row">
                                            <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Email</p>
                                            <p class="col-sm-9"><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></p>
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger" role="alert">
                                                <ul class="list-unstyled mb-0">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title d-flex justify-content-between pb-4">
                                            <span>Informations Personelles</span>
                                            <a class="edit-link" href="{{ route('compte.edit', $compte) }}"><i class="fa fa-edit mr-1"></i>Modifier</a>
                                        </h5>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Nom</p>
                                                        <p class="col-sm-9">{{ Auth::user()->first_name }}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Prénon</p>
                                                        <p class="col-sm-9">{{ Auth::user()->last_name }}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Email</p>
                                                        <p class="col-sm-9"><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></p>
                                                    </div>

                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Adresse</p>
                                                        <p class="col-sm-9">{{ Auth::user()->address }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Spécialité</p>
                                                        <p class="col-sm-9">{{ optional(Auth::user()->gestionInformations)->specialite ?? 'Non renseigné' }}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Langue</p>
                                                        <p class="col-sm-9">{{ optional(Auth::user()->gestionInformations)->langue ?? 'Non renseigné' }}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Experience</p>
                                                        <p class="col-sm-9">{{ optional(Auth::user()->gestionInformations)->experience ?? 'Non renseigné' }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <p class="col-sm-3 text-sm-right mb-0 mb-sm-3">Horaires</p>
                                                        <p class="col-sm-9">{{ optional(Auth::user()->gestionInformations)->horaires ?? 'Non renseigné' }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <p class="col-sm-1 text-sm-right mb-0 mb-sm-3">Bio :</p>
                                                <p class="col-sm-10">{{ optional(Auth::user()->gestionInformations)->bio ?? 'Non renseigné' }}</p>
                                            </div>
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger" role="alert">
                                                <ul class="list-unstyled mb-0">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div id="password_tab" class="tab-pane fade">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Modifier le mot de passe</h5>
                            <div class="row">
                                <div class="col-md-10 col-lg-6">
                                    <form action="{{ route('password.update.account') }}" method="post">
                                        @csrf
                                        @method('patch')
                                        <div class="form-group">
                                            <label>Ancien mot de passe</label>
                                            <input type="password" name="current_password" class="form-control">
                                        </div>
                                        @error('current_password')
                                        <p class="text-red-500 mt-2">{{ $message }}</p>
                                        @enderror
                                        <div class="form-group">
                                            <label>Nouveau mot de passe</label>
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                        @error('password')
                                        <p class="text-red-500 mt-2">{{ $message }}</p>
                                        @enderror
                                        <div class="form-group">
                                            <label>Confirmer mot de passe</label>
                                            <input type="password" name="password_confirmation" class="form-control">
                                        </div>
                                        @error('password_confirmation')
                                        <p class="text-red-500 mt-2">{{ $message }}</p>
                                        @enderror
                                        <button class="btn btn-primary" type="submit">Enregistrer les modifications</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="inforamtion_tab" class="tab-pane fade">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter Informations Personnelles</h5>
                            <div class="row">
                                <div class="col-md-10 col-lg-12">
                                    <form action="{{ route('compte.store') }}" method="post">
                                        @csrf
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Spécialité</label>
                                                        <input type="text" name="specialite" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Horaires</label>
                                                        <input type="text" name="horaires" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Langue</label>
                                                        <input type="text" name="langue" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Expérience</label>
                                                        <input type="text" name="experience" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Diplôme</label>
                                                        <input type="text" name="diplome" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Bio</label>
                                                        <textarea name="bio" class="form-control" rows="3" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button class="btn btn-primary" type="submit">Enregistrer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal for editing personal details -->
        <div class="modal fade" id="edit_personal_details" aria-hidden="true" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Personal Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('admin.profile.update') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="row form-row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Nom</label>
                                        <input type="text" class="form-control" name="first_name" value="{{ Auth::user()->first_name }}">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Prénom</label>
                                        <input type="text" class="form-control" name="last_name" value="{{ Auth::user()->last_name }}">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Photo de profile</label>
                                        <input type="file" name="avatar" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Enregistrer les modifications</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
