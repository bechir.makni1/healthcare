@extends('layouts.admin.app')

@section('title', 'Création - Rendez-vous')

@section('dashboard-content')
    <div class="container">
        <h1>Mon Calendrier</h1>
        @livewire('calendar')
    </div>
@endsection
