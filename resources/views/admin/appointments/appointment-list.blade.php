@extends('layouts.admin.app')

@section('title', 'List - Rendez-vous')

@section('dashboard-content')
    <div class="container">
        @livewire('nutritionist-appointments')
    </div>
@endsection

