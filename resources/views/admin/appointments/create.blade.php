@extends('layouts.admin.app')
@section('title','création - conseil')
@section('dashboard-header')
    <div class="container">
        <h1>Créer un Rendez-vous</h1>
@endsection
        <form method="POST" action="{{ route('appointments.store') }}">
            @csrf
            <div class="form-group">
                <label for="titre">Date</label>
                <input type="text" class="form-control" id="Date" name="Date" required>
            </div>
            <div class="form-group">
                <label for="date_debut">duree</label>
                <input type="datetime-local" class="form-control" id="duree" name="duree" required>
            </div>
            <div class="form-group">
                <label for="client_id">client_id</label>
                <input type="datetime-local" class="form-control" id="client_id" name="client_id" required>
            </div>
            <div class="form-group">
                <label for="nutritionniste_id">nutritionniste_id</label>
                <input type="datetime-local" class="form-control" id="nutritionniste_id" name="nutritionniste_id" required>
            </div>

            <button type="submit" class="btn btn-primary">Créer</button>
        </form>
    </div>
@endsection
