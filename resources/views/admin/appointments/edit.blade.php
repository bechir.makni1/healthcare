@extends('layouts.admin.app')

@section('title', 'Création - Rendez-vous')

@section('dashboard-header')
    <link href='https://cdn.jsdelivr.net/npm/@fullcalendar/core/main.css' rel='stylesheet' />
    <link href='https://cdn.jsdelivr.net/npm/@fullcalendar/daygrid/main.css' rel='stylesheet' />
    <link href='https://cdn.jsdelivr.net/npm/@fullcalendar/timegrid/main.css' rel='stylesheet' />
@endsection

@section('dashboard-content')
    @livewire('calendar')
@endsection

@push('after-scripts')
    <script src='https://cdn.jsdelivr.net/npm/@fullcalendar/core/main.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/@fullcalendar/daygrid/main.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/@fullcalendar/timegrid/main.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/@fullcalendar/interaction/main.js'></script>
    <script>
        document.addEventListener('livewire:load', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'dayGrid', 'timeGrid', 'interaction' ],
                initialView: 'dayGridMonth',
                events: @json($appointments)
            });
            calendar.render();

            window.addEventListener('refreshCalendar', event => {
                calendar.removeAllEvents();
                calendar.addEventSource(event.detail.appointments);
                calendar.refetchEvents();
            });
        });
    </script>
@endpush
