@extends('layouts.admin.app')
@section('title', "page d acceuil")
@section('dashboard-header')
    <div class="row">
        <div class="col-sm-12 mt-5">
            <h3 class="page-title mt-3">Bonjour, {{(\Illuminate\Support\Facades\Auth::user()->fullName())}}</h3>
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Dashboard</li>
            </ul>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card board1 fill">
                <div class="card-body">
                    <div class="dash-widget-header">
                        <div>
                            <h3 class="card_widget_header">{{\App\Models\User::all()->where('type',\App\Models\User::TYPE_NUTRITIONNISTE)->count() }}</h3>
                            <h6 class="text-muted">Total nutrisionniste</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                      <span class="opacity-7 text-muted"
                      ><svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewbox="0 0 24 24"
                              fill="none"
                              stroke="#009688"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              class="feather feather-user-plus"
                          >
                          <path
                              d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"
                          ></path>
                          <circle cx="8.5" cy="7" r="4"></circle>
                          <line x1="20" y1="8" x2="20" y2="14"></line>
                          <line x1="23" y1="11" x2="17" y2="11"></line></svg
                          ></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card board1 fill">
                <div class="card-body">
                    <div class="dash-widget-header">
                        <div>
                            <h3 class="card_widget_header">{{$categories}}</h3>
                            <h6 class="text-muted">Total Catégories</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                      <span class="opacity-7 text-muted"
                      ><svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewbox="0 0 24 24"
                              fill="none"
                              stroke="#009688"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              class="feather feather-dollar-sign"
                          >
                          <line x1="12" y1="1" x2="12" y2="23"></line>
                          <path
                              d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"
                          ></path></svg
                          ></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card board1 fill">
                <div class="card-body">
                    <div class="dash-widget-header">
                        <div>
                            @if(\Illuminate\Support\Facades\Auth::user()->type === \App\Models\User::TYPE_NUTRITIONNISTE)
                                <a href="{{ route('recette.index') }}">
                                    <h3 class="card_widget_header">{{ $nutritionniste_recettes }}</h3>
                                    <h6 class="text-muted">Total Recettes</h6>
                                </a>
                            @else
                                <a href="{{ route('recette.index') }}">
                                    <h3 class="card_widget_header">{{ $recettes->count() }}</h3>
                                    <h6 class="text-muted">Total Recettes</h6>
                                </a>
                            @endif

                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                      <span class="opacity-7 text-muted"
                      ><svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewbox="0 0 24 24"
                              fill="none"
                              stroke="#009688"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              class="feather feather-file-plus"
                          >
                          <path
                              d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"
                          ></path>
                          <polyline points="14 2 14 8 20 8"></polyline>
                          <line x1="12" y1="18" x2="12" y2="12"></line>
                          <line x1="9" y1="15" x2="15" y2="15"></line></svg
                          ></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card board1 fill">
                <div class="card-body">
                    <div class="dash-widget-header">
                        <div>
                            @if(\Illuminate\Support\Facades\Auth::user()->type === \App\Models\User::TYPE_NUTRITIONNISTE)
                                <a href="{{ route('conseil.index') }}">
                                    <h3 class="card_widget_header">{{ $nutritionniste_conseils }}</h3>
                                    <h6 class="text-muted">Total Conseils</h6>
                                </a>
                            @else
                                <a href="{{ route('conseil.index') }}">
                                    <h3 class="card_widget_header">{{ $conseils->count() }}</h3>
                                    <h6 class="text-muted">Total Conseils</h6>
                                </a>
                            @endif
                        </div>

                        <div class="ml-auto mt-md-3 mt-lg-0">
                      <span class="opacity-7 text-muted"
                      ><svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewbox="0 0 24 24"
                              fill="none"
                              stroke="#009688"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              class="feather feather-globe"
                          >
                          <circle cx="12" cy="12" r="10"></circle>
                          <line x1="2" y1="12" x2="22" y2="12"></line>
                          <path
                              d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"
                          ></path></svg
                          ></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
