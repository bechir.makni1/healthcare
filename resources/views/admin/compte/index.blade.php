@extends('layouts.admin.app')
@section('title', "informations-compte")
@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Informations de compte </h4>
                </div>
            </div>
            @endsection
            @section('dashboard-content')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-table">
                            <div class="card-body booking_card">
                                <div class="table-responsive">
                                    <table class="datatable table table-stripped table table-hover table-center mb-0">
                                        <thead>
                                        <tr>
                                            <th>Auteur</th>
                                            <th>adresse</th>
                                            <th>Specialité</th>
                                            <th>Langue</th>
                                            <th>horaires</th>
                                            <th>Date</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($comptes as $compte)
                                            <tr>
                                                <td>
                                                    <h2 class="table-avatar">
                                                        <a href="{{ route('admin.profile.edit') }}" class="avatar avatar-sm mr-2">
                                                            <div class="author-img-wrap">
                                                                <img class="img-fluid rounded-circle" alt="" src="{{ asset($compte->user->avatar) }}">
                                                            </div>
                                                        </a>
                                                        <a href="{{ route('admin.profile.edit') }}">{{ $compte->user->fullName() }}</a>
                                                    </h2>
                                                </td>
                                                <td>{{$compte->adresse}}</td>
                                                <td>{{$compte->specialite}}</td>
                                                <td>{{$compte->langue}}</td>
                                                <td>{{$compte->horaires}}</td>
                                                <td>
                                                    @php
                                                        $time=Carbon\Carbon::now()
                                                    @endphp
                                                    {{$time->dayName}}, {{$time->isoFormat('LL')}}
                                                </td>
                                                <td class="text-right">
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-v ellipse_color"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('compte.show', $compte) }}">
                                                                <i class="fas fa-pencil-alt m-r-5"></i> voir
                                                            </a>
                                                            <a class="dropdown-item" href="{{ route('compte.edit', $compte) }}">
                                                                <i class="fas fa-pencil-alt m-r-5"></i> modifier
                                                            </a>
                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#delete_compte_{{ $compte->id }}">
                                                                <i class="fas fa-trash-alt m-r-5"></i> Supprimer
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <!-- Delete Modal -->
                                            <div id="delete_compte_{{ $compte->id }}" class="modal fade delete-modal" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-body text-center">
                                                            <img src="{{ asset('assets/img/sent.png') }}" alt="" width="50" height="46">
                                                            <h3 class="delete_class">Etes vous sure de vouloir supprimer cet element?</h3>
                                                            <div class="m-t-20">
                                                                <form action="{{ route('compte.destroy', $compte) }}" method="POST">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="submit" class="btn btn-danger">Supprimer</button>
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Fermer</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
