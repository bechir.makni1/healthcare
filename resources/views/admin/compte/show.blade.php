@extends('layouts.admin.app')
@section('title', "informations-compte")
@section('dashboard-header')
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Details de compte</h4>
        </div>
        @endsection
        @section('dashboard-content')
            <div class="row mt-3">
                <div class="col-md-8">
                    <div class="blog-view">
                        <article class="blog blog-single-post">
                            <div class="blog-content mt-4">
                                <h4><b>Biographie </b></h4>
                                <p>{{$compte->bio}}</p>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>Experiencce: </b></h4>
                                <p>{{$compte->experience}}</p>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>Horraire: </b></h4>
                                <p>{{$compte->horaires}}</p>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>Diplome: </b></h4>
                                <p>{{$compte->diplome}}</p>
                            </div>
                        </article>
                        <div class="widget author-widget clearfix">
                            <h3>A propos de l'auteur</h3>
                            <div class="about-author">
                                <div class="about-author-img">
                                    <div class="author-img-wrap">
                                        <img class="img-fluid" alt="" src="{{ asset($compte->user->avatar) }}">
                                    </div>
                                </div>
                                <div class="author-details"><span
                                        class="blog-author-name">{{$compte->user->fullName()}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
