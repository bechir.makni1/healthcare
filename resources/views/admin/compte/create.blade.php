@extends('layouts.admin.app')
@section('title','Gestion de Compte')
@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">
                Informations de Compte
            </h3>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{route('compte.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="last_name">Adresse</label>
                            <input
                                id="adresse"
                                class="form-control"
                                type="text"
                                name="adresse"
                                required
                            />
                            @error('adresse')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Bio</label>
                            <textarea class="form-control" rows="5" name="bio"></textarea>
                            @error('bio')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="address">Spécialité</label>
                            <input
                                id="specialite"
                                class="form-control"
                                type="text"
                                name="specialite"
                                required
                            />
                            @error('specialite')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Horaires</label>
                            <input
                                id="horaires"
                                class="form-control"
                                type="text"
                                name="horaires"
                            />
                            @error('horaires')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Langue</label>
                            <input
                                id="langue"
                                class="form-control"
                                type="text"
                                name="langue"
                            />
                            @error('langue')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Diplome</label>
                            <input
                                id="diplome"
                                class="form-control"
                                type="text"
                                name="diplome"
                            />
                            @error('diplome')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Experience</label>
                            <textarea class="form-control" rows="5" name="experience"></textarea>
                            @error('experience')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary buttonedit1">
                    Enregistrer
                </button>
            </form>
        </div>
    </div>

@endsection

