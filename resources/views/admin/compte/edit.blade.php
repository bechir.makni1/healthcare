@extends('layouts.admin.app')

@section('title', 'Modification des informations')

@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Modifier les informations de compte</h3>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('compte.update', $compte->id) }}" method="POST" enctype="multipart/form-data">

                @csrf
                @method('PUT')
                <div class="row formtype">


                    <div class="col-md-4">
                        <div class="form-group">
                            <label>prenom</label>
                            <input type="text" class="form-control" name="last_name" value="{{(auth()->user()->last_name)}}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control" name="first_name" value="{{(auth()->user()->first_name)}}"> </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" value={{(auth()->user()->email)}}> </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Photo de profile</label>
                            <input type="file" name="avatar" class="form-control"> </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Addresse</label>
                            <input class="form-control" type="text" name="address" value="{{ old('address', auth()->user()->address) }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Biographie</label>
                            <textarea class="form-control" rows="5" name="bio">{{ old('bio', $compte->bio) }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Specialité</label>
                            <input class="form-control" type="text" name="specialite" value="{{ old('specialite', $compte->specialite) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>horaires</label>
                            <input class="form-control" type="text" name="horaires" value="{{ old('horaires', $compte->horaires) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>langue</label>
                            <input class="form-control" type="text" name="langue" value="{{ old('langue', $compte->langue) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Diplome</label>
                            <input class="form-control" type="text" name="diplome" value="{{ old('diplome', $compte->diplome) }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Experience</label>
                            <textarea class="form-control" rows="5" name="experience">{{ old('experience', $compte->experience) }}</textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary buttonedit1">Enregistrer modifictaions</button>
            </form>
        </div>
    </div>
@endsection
