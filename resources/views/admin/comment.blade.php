@extends('layouts.admin.app')
@section('title', "page  commentaires")
@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Commentaires</h4>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body booking_card">
                    <div class="table-responsive">
                        <table class="datatable table table-stripped table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>Auteur</th>
                                <th>Article</th>
                                <th>Message</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $comment)
                                <tr>
                                    <td><h2 class="table-avatar">
                                            <a
                                                class="avatar avatar-sm mr-2">
                                                <img
                                                    class="avatar-img rounded-circle"
                                                    src="{{ asset($comment->user->avatar) }}"
                                                    alt="User Image" />
                                            </a>
                                            <a href="">{{$comment->user->fullName()}}</a>
                                        </h2>
                                    </td>
                                    <td>
                                        @if($comment->recette)
                                            {{$comment->recette->title}}
                                        @elseif($comment->conseil)
                                            {{$comment->conseil->title}}
                                        @else
                                            Pas de titre disponible
                                        @endif
                                    </td>
                                    <td>{{Illuminate\Support\Str::limit($comment->content), 25}}</td>
                                    <td>
                                        @php $time=$comment->created_at@endphp
                                        {{$time->isoFormat('LL')}}
                                    </td>
                                    <td class="text-center">
                                            <span class="badge badge-pill bg-success inv-badge">
                                                {{ $comment->isActive == 1 ? "ACTIVE" : "DÉSACTIVÉ" }}
                                            </span>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown dropdown-action"> <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v ellipse_color"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="">
                                                    <form action="{{ route('comment.update',$comment)}}" method="post">
                                                        @csrf
                                                        @method('PUT')
                                                        <button type="submit" class="btn btn_link">
                                                            <i class="fas fa-pencil-alt m-r-5"></i> bloquer
                                                        </button>
                                                    </form>
                                                </a>
                                                    <a class="dropdown-item" href="">
                                                    <form action="{{ route('comment.debloquer',$comment)}}" method="post">
                                                        @csrf
                                                        @method('PUT')
                                                        <button type="submit" class="btn btn_link">
                                                            <i class="fas fa-pencil-alt m-r-5"></i> debloquer
                                                        </button>
                                                    </form>
                                                    </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="delete_asset" class="modal fade delete-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center"> <img src="assets/img/sent.png" alt="" width="50" height="46">
                    <h3 class="delete_class">Etes vous sure de vouloir supprimer cet element?</h3>
                    <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
