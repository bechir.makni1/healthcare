<div class="header">
    <div class="header-left">
        <a href="index.html" class="logo">
            @if(is_null(auth()->user()->avatar))
                <img class="rounded-circle" alt="User Image" src="{{asset('admin/assets/img/profiles/avatar-01.png')}}">
            @else
                <img
                    src="{{asset(auth()->user()->avatar)}}"
                    width="50"
                    height="70"
                    alt="logo"
                />
            @endif
            <span class="logoclass">{{(auth()->user()->first_name)}}</span>
        </a>
        <a href="index.html" class="logo logo-small">
            @if(auth()->user()->avatar === null)
                <img class="rounded-circle" alt="User Image" src="{{asset('admin/assets/img/profiles/avatar-01.png')}}">
            @else
                <img
                    src="{{asset(auth()->user()->avatar)}}"
                    alt="Logo"
                    width="30"
                    height="30"
                />
            @endif
        </a>
    </div>
    <a href="javascript:void(0);" id="toggle_btn">
        <i class="fe fe-text-align-left"></i>
    </a>
    <a class="mobile_btn" id="mobile_btn"> <i class="fas fa-bars"></i> </a>
    <ul class="nav user-menu">
        <li class="nav-item dropdown has-arrow">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
    <span class="user-img">
        @if(auth()->user()->avatar == null)
            <img class="rounded-circle" alt="User Image" src="{{ asset('admin/assets/img/profiles/avatar-01.png') }}"
                 width="30" height="30">
        @else
            <img class="rounded-circle" src="{{ asset(auth()->user()->avatar) }}" width="30" height="30"
                 alt="{{ auth()->user()->first_name }}">
        @endif
    </span>
            </a>

            <div class="dropdown-menu">
                <div class="user-header">
                    <div class="avatar avatar-sm">
                        @if(auth()->user()->avatar === null)
                            <img class="rounded-circle" alt="User Image"
                                 src="{{asset('admin/assets/img/profiles/avatar-01.png')}}">
                        @else
                            <img
                                src="{{asset(auth()->user()->avatar)}}"
                                alt="User Image"
                                class="avatar-img rounded-circle"
                            />
                        @endif
                    </div>
                    <div class="user-text">
                        <h6>{{ auth()->user()->first_name }}</h6>
                        <p class="text-muted mb-0">
                            @if(auth()->user()->type === \App\Models\User::TYPE_SUPER_ADMIN)
                                <small> Super Administrateur</small>
                            @elseif(auth()->user()->type === \App\Models\User::TYPE_NUTRITIONNISTE)
                                <small>Nutritionniste</small>
                            @endif
                        </p>
                    </div>
                </div>
                <a class="dropdown-item" href="{{route('admin.profile.edit')}}">Profile</a>
                <a class="dropdown-item" href="{{route('settings.index')}}">Paramettre</a>
                <a class="dropdown-item" href="{{route('logout')}}">logout</a>
            </div>
        </li>
    </ul>
</div>

