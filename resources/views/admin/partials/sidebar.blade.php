<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="active">
                    <a href="{{route('home')}}"
                    ><i class="fas fa-tachometer-alt"></i>
                        <span>Acceuil
                        </span>
                    </a>
                </li>

                <li class="list-divider"></li>

                @can('access-admin')
                    <li class="submenu">
                        <a href="#"><i class="fas fa-user"></i> <span> Auteurs </span>
                            <span class="menu-arrow"></span></a>
                        <ul class="submenu_class" style="display: none">
                            <li><a href="{{route('nutritionniste.index')}}">Nutritionniste </a>
                            </li>
                            <li>
                                <a href="{{route('client.index')}}">Client </a>
                            </li>
                        </ul>
                    </li>
                @endcan
                @if(auth()->check() && auth()->user()->type == \App\Models\User::TYPE_NUTRITIONNISTE)
                <li>
                    <a
                    ><i class="fa fa-calendar"></i> <span> Rendez-vous </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="submenu_class" style="display: none">
                        <li><a href="{{route('appointments.index')}}" > Calendrier </a></li>
                        <li><a href="{{route('appointments.list')}}"> liste </a></li>
                    </ul>
                </li>
                @endif
                @can('access-admin')
                    <li>
                        <a href="{{route('category.index')}}">
                            <i class="fas fa-book"></i></i>
                            <span>Catégories</span>
                        </a>
                    </li>
                @endcan
                <li>
                    <a href="{{route('recette.index')}}"
                    ><i class="fas fa-edit"></i> <span>Recettes</span></a
                    >
                </li>
                <li>
                    <a href="{{route('conseil.index')}}"
                    ><i class="fas fa-edit"></i> <span>Conseils</span></a
                    >
                </li>
                 @can('access-admin')
                    <li>
                        <a href="{{route('produit.index')}}"
                        ><i class="fas fa-edit"></i> <span>Produits</span></a
                        >
                    </li>
                @endcan
                @can('access-admin')
                    <li>
                        <a href="{{route('commande.admin')}}"
                        ><i class="fe fe-table"></i> <span>Tous Les Commandes</span></a
                        >
                    </li>
                @endcan
                <li>
                    <a href="{{route('comment.index')}}"
                    ><i class="fe fe-table"></i> <span>Commentaires</span></a
                    >
                </li>
                @can('access-admin')
                  <li>
                     <a href="{{route('settings.index')}}"
                    ><i class="fas fa-cog"></i> <span>Paramètres</span></a
                    >
                  </li>
                @endcan
            </ul>
        </div>
    </div>
</div>
