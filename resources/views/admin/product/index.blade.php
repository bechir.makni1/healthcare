@extends('layouts.admin.app')
@section('title', "Liste des Poduits")

@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <div class="mt-5">
                <h4 class="card-title float-left mt-2">Produits</h4>
                <a href="{{ route('produit.create') }}" class="btn btn-primary float-right veiwbutton">Ajouter un produit</a>
            </div>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body booking_card">
                    <div class="table-responsive">
                        <table class="datatable table table-stripped table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>Auteur</th>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>Date</th>
                                <th>Prix</th>
                                <th>Livraison</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($produits as $produit)
                                <tr>
                                    <td>
                                        <h2 class="table-avatar">
                                            <a href="{{ route('admin.profile.edit') }}" class="avatar avatar-sm mr-2">
                                                <div class="author-img-wrap">
                                                    <img class="img-fluid rounded-circle" alt="" src="{{ asset($produit->user->avatar) }}"> </div>
                                            </a>
                                            <a href="{{ route('admin.profile.edit') }}">{{ $produit->user->fullName() }}</a>
                                        </h2>
                                    </td>
                                    <td>
                                        <img src="{{ asset($produit->image)  }}" alt="{{ $produit->name }}" width="100" height="100">
                                    </td>
                                    <td>{{ $produit->name }}</td>
                                    <td>{{$produit->created_at}}</td>
                                    <td>{{ $produit->price }}</td>
                                    <td>{{ $produit->livraison }}</td>

                                    <td class="text-right">
                                        <div class="dropdown dropdown-action">
                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v ellipse_color"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="{{ route('produit.show', $produit) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> Voir
                                                </a>
                                                <a class="dropdown-item" href="{{ route('produit.edit', $produit) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> Modifier
                                                </a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#delete_recette_{{ $produit->id }}">
                                                    <i class="fas fa-trash-alt m-r-5"></i> Supprimer
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <!-- Delete recipe modal -->
                                <div id="delete_recette_{{ $produit->id }}" class="modal fade delete-modal" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-body text-center">
                                                <img src="{{asset('admin/assets/img/sent.png')}}" alt="" width="50" height="46">
                                                <h3 class="delete_class">Etes-vous sûr de vouloir supprimer cet élément?</h3>
                                                <div class="m-t-20">
                                                    <form action="{{ route('produit.destroy', $produit) }}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Fermer</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
