@extends('layouts.admin.app')
@section('title', "L'ajout d'un produit")
@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Ajouter un produit</h3>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route("produit.store") }}" method="POST" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nom du Produit</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" required>
                            @error('name')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Prix</label>
                            <input class="form-control" type="number" step="0.01" min="0.00" name="price" value="{{ old('price') }}" required>
                            @error('price')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>livraison</label>
                            <input class="form-control" type="number" step="0.01" min="0.00" name="livraison" value="{{ old('livraison') }}" required>
                            @error('livraison')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="image" required>
                                <label class="custom-file-label" for="customFile">Choisir une image</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Détails du Produit</label>
                            <input class="form-control" type="text" name="details" value="{{ old('details') }}">
                            @error('details')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="5" name="description">{{ old('description') }}</textarea>
                            @error('description')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Enregistrer de produit</button>
            </form>
        </div>
    </div>
@endsection
