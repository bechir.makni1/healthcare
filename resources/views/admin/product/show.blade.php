@extends('layouts.admin.app')
@section('title', "liste des Produits")
@section('dashboard-header')
<br/>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Details de Produit</h4>
        </div>
        @endsection
        @section('dashboard-content')
            <div class="row mt-3">
                <div class="col-md-8">
                    <div class="blog-view">
                        <article class="blog blog-single-post">
                            <h3 class="blog-title">{{$produit->name}}</h3>
                            <div class="blog-image">
                                <a href="blog-details.html"><img alt="" src="{{$produit->imageUrl()}}"
                                                                 class="img-fluid mt-4"></a>
                            </div>

                            <div class="blog-content mt-4">
                                <h4><b>Description: </b></h4>
                                <p> {{$produit->description}}</p>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>Détails:</b></h4>
                                <p>{{$produit->details}}</p>

                            </div>
                        </article>
                        <div class="widget author-widget clearfix">
                            <h3>A propos de l'auteur</h3>
                            <div class="about-author">
                                <div class="about-author-img">
                                    <div class="author-img-wrap">
                                        <img class="img-fluid" alt="" src="{{ asset($produit->user->avatar) }}">
                                    </div>
                                </div>
                                <div class="author-details"><span
                                        class="blog-author-name">{{$produit->user->fullname()}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection
