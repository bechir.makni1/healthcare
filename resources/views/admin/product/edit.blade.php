@extends('layouts.admin.app')

@section('title', 'Modification de Produit')

@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Modifier un produit</h3>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('produit.update', ['produit' => $produit->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row formtype">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nom de Produit</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name', $produit->name) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Prix de Produit</label>
                            <input class="form-control" type="number" step="0.01" min="0.00" name="price" value="{{ old('price', $produit->price) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>livraison</label>
                            <input class="form-control" type="number" step="0.01" min="0.00" name="livraison" value="{{ old('livraison', $produit->livraison) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="image">
                                <label class="custom-file-label" for="customFile">Choisir une image</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="5" name="description">{{ old('description', $produit->description) }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Details</label>
                            <textarea class="form-control" rows="5" name="details">{{ old('details', $produit->details) }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary buttonedit1">Enregistrer produit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
