@extends('layouts.admin.app')
@section('title', "details de conseil")
@section('dashboard-header')
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Details de conseil</h4>
        </div>
        @endsection
        @section('dashboard-content')
            <div class="row mt-3">
                <div class="col-md-8">
                    <div class="blog-view">
                        <article class="blog blog-single-post">
                            <h3 class="blog-title">{{$conseil->titre}}</h3>
                            <div class="blog-image">
                                <a href="blog-details.html"><img alt="" src="{{$conseil->imageUrl()}}"
                                                                 class="img-fluid mt-4"></a>
                            </div>
                            <div class="blog-content mt-4">
                                <h4><b>contenu de conseil: </b></h4>
                                <p>{{$conseil->contenu}}</p>
                            </div>

                        </article>
                        <div class="widget author-widget clearfix">
                            <h3>A propos de l'auteur</h3>
                            <div class="about-author">
                                <div class="about-author-img">
                                    <div class="author-img-wrap">
                                        <img class="img-fluid" alt="" src="{{ asset($conseil->user->avatar) }}">
                                    </div>
                                </div>
                                <div class="author-details"><span
                                        class="blog-author-name">{{$conseil->user->fullName()}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
