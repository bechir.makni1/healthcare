@extends('layouts.admin.app')
@section('title', "liste des conseil")
@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <div class="mt-5">
                <h4 class="card-title float-left mt-2">Conseils</h4>
                <a href="{{ route('conseil.create') }}" class="btn btn-primary float-right veiwbutton">Ajouter un
                    conseil</a>
            </div>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body booking_card">
                    <div class="table-responsive">
                        <table class="datatable table table-stripped table-hover table-center mb-3 p-3">
                            <thead>
                            <tr>
                                <th>Auteur</th>
                                <th>Image</th>
                                <th>Titre</th>
                                <th>Contenu</th>
                                <th>Publication</th>
                                <th>Commentaires</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            @foreach($conseils as $conseil)
                                <tbody>
                                <tr>
                                    <td>
                                        <h2 class="table-avatar">
                                            <a href="{{ route('admin.profile.edit') }}" class="avatar avatar-sm mr-2">
                                                <div class="author-img-wrap">
                                                    <img class="img-fluid rounded-circle" alt="" src="{{ asset($conseil->user->avatar) }}">
                                                </div>
                                            </a>
                                            <a href="{{ route('admin.profile.edit') }}">{{ $conseil->user->fullName() }}</a>
                                        </h2>
                                    </td>
                                    <td>
                                        <img src="{{ asset($conseil->image)  }}" alt="Conseil" width="100" height="100">
                                    </td>
                                    <td>{{ $conseil->titre }}</td>

                                    <td>{{Illuminate\Support\Str::limit( $conseil->contenu), 25}}</td>

                                    <td>
                                        <div class="actions">
                                            @if($conseil->isActive == 1)
                                                <a href="#" class="btn btn-sm bg-success-light mr-2">Publié</a>
                                            @elseif($conseil->isActive == 0)
                                                <a href="#" class="btn btn-sm bg-success-light mr-2">Non Publié</a>
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <div class="actions">
                                            @if($conseil->isComment == 1)
                                                <a href="#" class="btn btn-sm bg-success-light mr-2">Active</a>
                                            @elseif($conseil->isComment == 0)
                                                <a href="#" class="btn btn-sm bg-success-light mr-2">Desactivé</a>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown dropdown-action">
                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-v ellipse_color"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right text-right">
                                                <a class="dropdown-item" href="{{ route('conseil.show', $conseil) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> Voir
                                                </a>
                                                <a class="dropdown-item" href="{{ route('conseil.edit', $conseil) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> Modifier</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#delete_conseil_{{ $conseil->id }}">
                                                    <i class="fas fa-trash-alt m-r-5"></i> Supprimer</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>

                                <!-- Delete Modal -->
                                <div id="delete_conseil_{{ $conseil->id }}" class="modal fade delete-modal" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-body text-center">
                                                <img src="{{asset('admin/assets/img/sent.png')}}" alt="" width="50" height="46">
                                                <h3 class="delete_class">Etes vous sure de vouloir supprimer cet element?</h3>
                                                <div class="m-t-20">
                                                    <form action="{{ route('conseil.destroy', ['conseil' => $conseil->id]) }}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Fermer</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Delete Modal -->
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
