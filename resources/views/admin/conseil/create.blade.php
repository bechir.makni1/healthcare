
@extends('layouts.admin.app')
@section('title',"liste des conseil")
@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Ajouter une conseil</h3>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">

        <div class="col-lg-12">
            <form action="{{ route('conseil.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row formtype">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Titre de conseil</label>
                            <input class="form-control" type="text" name="titre" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contenu de conseil</label>
                            <textarea class="form-control" rows="5" name="contenu"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="image" />
                                <label class="custom-file-label" for="customFile">Choisir une image</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Publication</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="article_active" name="isActive" value="1" checked>
                                <label class="form-check-label" for="article_active">Publier</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="article_inactive" name="isActive" value="0">
                                <label class="form-check-label" for="article_inactive">Ne pas publier</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Commentaires</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="article_comment_active" name="isComment" value="1" checked>
                                <label class="form-check-label" for="article_comment_active">Autorise</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="article_comment_inactive" name="isComment" value="0">
                                <label class="form-check-label" for="article_comment_inactive">Non autorise</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary buttonedit1">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
