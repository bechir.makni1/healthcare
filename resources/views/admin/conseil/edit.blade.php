
@extends('layouts.admin.app')

@section('title', 'Modification de conseil')

@section('dashboard-header')
    <div class="row align-items-center">
        <div class="col">
            <h3 class="page-title mt-5">Modifier une conseil</h3>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('conseil.update', $conseil->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row formtype">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Titre de conseil</label>
                            <input class="form-control" type="text" name="titre" value="{{ old('titre', $conseil->titre) }}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uploader une image</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="image">
                                <label class="custom-file-label" for="customFile">Choisir une image</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Contenu</label>
                            <textarea class="form-control" rows="5" name="contenu">{{ old('contenu', $conseil->contenu) }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Publication</label>
                            <!-- Ajout de la condition old() pour vérifier si la valeur est ancienne -->
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="article_active" name="isActive" value="1" {{ old('isActive', $conseil->isActive) == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="article_active">Publier</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="article_inactive" name="isActive" value="0" {{ old('isActive', $conseil->isActive) == 0 ? 'checked' : '' }}>
                                <label class="form-check-label" for="article_inactive">Ne pas publier</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Partages</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="article_share_active" name="isSharable" value="1" {{ old('isSharable', $conseil->isSharable) == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="article_share_active">Partageable</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="article_share_inactive" name="isSharable" value="0" {{ old('isSharable', $conseil->isSharable) == 0 ? 'checked' : '' }}>
                                <label class="form-check-label" for="article_share_inactive">Non Partageable</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Commentaires</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="article_comment_active" name="isComment" value="1" {{ old('isComment', $conseil->isComment) == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="article_comment_active">Autorise</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="article_comment_inactive" name="isComment" value="0" {{ old('isComment', $conseil->isComment) == 0 ? 'checked' : '' }}>
                                <label class="form-check-label" for="article_comment_inactive">Non autorise</label>
                            </div>
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary buttonedit1">Enregistrer conseil</button>
            </form>
        </div>
    </div>
@endsection
