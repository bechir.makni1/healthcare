@extends('layouts.admin.app')

@section('title', 'Modification de catégorie')

@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Modifier une catégorie</h4>
                    <a href="{{ route('category.index') }}" class="btn btn-primary float-right veiwbutton">Retour</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body">
                    <form action="{{ route('category.update',$category) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Nom de la catégorie</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name', $category->name) }}" required>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3">{{ old('description', $category->description) }}</textarea>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="isActive">Statut</label>
                            <select class="form-control @error('isActive') is-invalid @enderror" id="isActive" name="isActive">
                                <option value="1" {{ old('isActive', $category->isActive) == 1 ? 'selected' : '' }}>Activer</option>
                                <option value="0" {{ old('isActive', $category->isActive) == 0 ? 'selected' : '' }}>Désactiver</option>
                            </select>
                            @error('isActive')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
