@extends('layouts.admin.app')

@section('title', 'Modification de client')

@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Modifier un client</h4>
                    <a href="{{ route('client.index') }}"
                       class="btn btn-primary float-right veiwbutton">Retour</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body">
                    <form action="{{ route('client.update',['client' => $client]) }}"  method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name"
                                   name="first_name" value="{{ old('first_name', $client->first_name) }}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">prenom</label>
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name"
                                   name="last_name" value="{{ old('last_name', $client->last_name) }}" required>
                            @error('prenom')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">email </label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                                   name="email" value="{{ old('email', $client->email) }}" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">password </label>
                            <input type="text" class="form-control @error('password') is-invalid @enderror"
                                   id="password" name="password"
                                   value="{{ old('password', $client->password) }}" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">adresse </label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" id="address"
                                   name="address" value="{{ old('address', $client->address) }}" required>
                            @error('adresse')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">telephone </label>
                            <input type="text" class="form-control @error('telephone') is-invalid @enderror"
                                   id="telephone" name="phone"
                                   value="{{ old('phone', $client->phone) }}" required>
                            @error('telephone')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="isActive">Statut</label>
                            <select class="form-control @error('isActive') is-invalid @enderror" id="isActive"
                                    name="isActive">
                                <option
                                    value="1" {{ old('isActive', $client->isActive) == 1 ? 'selected' : '' }}>
                                    Activer
                                </option>
                                <option
                                    value="0" {{ old('isActive', $client->isActive) == 0 ? 'selected' : '' }}>
                                    Désactiver
                                </option>
                            </select>
                            @error('isActive')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
