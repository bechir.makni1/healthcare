@extends('layouts.admin.app')
@section('title', 'Dashboard - client')
@section('dashboard-header')
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <div class="mt-5">
                    <h4 class="card-title float-left mt-2">Client</h4>
                    <a href="{{ route('client.create') }}" class="btn btn-primary float-right veiwbutton">
                        Ajouter un client</a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('dashboard-content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-body booking_card">
                    <div class="table-responsive">
                        <table class="datatable table table-stripped table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>prenom</th>
                                <th>email</th>
                                <th>adresse</th>
                                <th>telephone</th>
                                <th>status</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td><img src="{{ asset($client->avatar) }}" alt="Avatar du client" alt= width="100" height="100"></td>
                                    <td>{{ $client->first_name }}</td>
                                    <td>{{ $client->last_name }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->address }}</td>
                                    <td>{{ $client->phone }}</td>
                                    <td class="text-center">
                                            <span class="badge badge-pill bg-success inv-badge">
                                                {{ $client->isActive == 1 ? "ACTIVE" : "DÉSACTIVÉ" }}
                                            </span>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown dropdown-action">
                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v ellipse_color"></i>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="{{ route('client.show', $client->id) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> voir
                                                </a>
                                                <a class="dropdown-item" href="{{ route('client.edit', $client->id) }}">
                                                    <i class="fas fa-pencil-alt m-r-5"></i> Modifier
                                                </a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#delete_category_{{ $client->id }}">
                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="fas fa-trash-alt m-r-5"></i> Supprimer
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div id="delete_category_{{ $client->id }}" class="modal fade delete-modal" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-body text-center">
                                                <img src=" {{asset('admin/assets/img/sent.png')}}" alt="" width="50" height="46" />
                                                <h3 class="delete_class">
                                                    Êtes-vous sûr de vouloir supprimer ce client ?
                                                </h3>
                                                <div class="m-t-20">
                                                    <form action="{{ route('client.destroy', $client) }}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Fermer</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
