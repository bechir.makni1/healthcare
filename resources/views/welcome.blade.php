@extends('layouts.appfront')
@section('content')
    <style>
        .featurs-item {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            height: 100%;
        }

        .featurs-icon img {
            height: 100px;
            width: 100px;
            border-radius: 50%;
        }

        .featurs-content {
            flex-grow: 1;
        }

        .featurs-item h5 {
            min-height: 3em; /* Adjust this value based on your content */
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
        }

        .fruite-item {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            height: 100%;
        }

        .fruite-img img {
            height: 200px;
            width: 100%;
            object-fit: cover;
        }

        .fruite-content {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .fruite-content h4 {
            min-height: 3em; /* Ajustez cette valeur selon vos besoins */
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
        }

        .fruite-content p {
            flex-grow: 1;
        }
    </style>
    <!-- Hero Start -->
    <div class="container-fluid py-5 mb-5 hero-header">
        <div class="container py-5">
            <div class="row g-5 align-items-center">
                <div class="col-md-12 col-lg-7">
                    <h4 class="mb-3 text-secondary">100% Biologiques</h4>
                    <h1 class="mb-5 display-3 text-primary">Santé et saveurs réunies pour une vie meilleure</h1>

                </div>
                <div class="col-md-12 col-lg-5">
                    <div id="carouselId" class="carousel slide position-relative" data-bs-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active rounded">
                                <img src="img/hero-img-1.png" class="img-fluid w-100 h-100 bg-secondary rounded"
                                     alt="First slide">
                                <a href="#" class="btn px-4 py-2 text-white rounded">Fruits</a>
                            </div>
                            <div class="carousel-item rounded">
                                <img src="img/hero-img-2.jpg" class="img-fluid w-100 h-100 rounded" alt="Second slide">
                                <a href="#" class="btn px-4 py-2 text-white rounded">Légumes</a>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselId"
                                data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselId"
                                data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero End -->

    <!-- Les conseils -->

    <div class="container-fluid featurs py-5">
        <div class="container py-5">
            <h1>les dernières conseils</h1>
            <br>
            <div class="row g-4">
                @foreach($conseils as $conseil)
                    <div class="col-md-6 col-lg-3">
                        <div class="featurs-item text-center rounded bg-light p-4">
                            <div class="featurs-icon btn-square rounded-circle bg-secondary mb-5 mx-auto">
                                <img src="{{ asset($conseil->image) }}" alt="Conseil Image">
                            </div>
                            <div class="featurs-content text-center">
                                <a class="d-block h5 lh-base mb-4"
                                   href="{{ route('front.conseildetails.index', ['id' => $conseil->id]) }}">
                                    <h5>{{$conseil->titre}}</h5>
                                </a>
                                <p class="mb-0">
                                    <i class="fa fa-user text-primary me-2"></i> {{$conseil->user->fullName()}}</p>
                                <small class="me-3"><i class="fa fa-calendar text-primary me-2"></i>
                                    {{ \Carbon\Carbon::parse($conseil->created_at)->format('m-d')}}
                                </small>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- end conseils -->

    <!-- les recettes-->
    <div class="container-fluid fruite py-5">
        <div class="container py-5">
            <div class="tab-class text-center">
                <div class="row g-4">
                    <div class="col-lg-4 text-start">
                        <h1>Les dernières recettes</h1>
                    </div>
                    <div class="col-lg-8 text-end">
                        <ul class="nav nav-pills d-inline-flex text-center mb-5">
                            @foreach($categories as $category)
                                <li class="nav-item">
                                    <a class="d-flex m-2 py-2 bg-light rounded-pill"
                                       href="{{ route('category.recette', $category->slug) }}">
                                        <span class="text-dark" style="width: 130px;">{{ ucfirst($category->name) }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane fade show p-0 active">
                        <div class="row g-4">
                            <div class="col-lg-12">
                                <div class="row g-4">
                                    @foreach($recettes as $recette)
                                        <div class="col-md-6 col-lg-4 col-xl-3">
                                            <div class="rounded position-relative fruite-item">
                                                <div class="fruite-img">
                                                    <img src="{{ asset($recette->image) }}"
                                                         class="img-fluid w-100 rounded-top" alt="">
                                                </div>
                                                <div class="text-white bg-secondary px-3 py-1 rounded position-absolute"
                                                     style="top: 10px; left: 10px;">{{ $recette->category->name }}</div>
                                                <div
                                                    class="p-4 border border-secondary border-top-0 rounded-bottom fruite-content">
                                                    <h4>{{ $recette->title }}</h4>
                                                    <p>{{ Illuminate\Support\Str::limit($recette->description, 100) }}</p>
                                                    <div class="d-flex justify-content-between flex-lg-wrap">
                                                        <p class="mb-0"><i class="fa fa-user text-primary me-2"></i>{{ $recette->user->fullName() }}</p>
                                                        <a href="{{ route('front.recettedetails.index', $recette->id) }}"
                                                           class="btn border border-secondary rounded px-3 text-primary">Voir
                                                            Plus</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End recette-->


    <!-- nutritionniste Start -->
    <div class="container-fluid service py-5">
        <div class="container py-5">
            <div class="col-lg-8 text-start">
                <h1>Nos meilleurs nutritionnistes</h1>
                <br>
            </div>
            <div class="row g-4 justify-content-center">

                @foreach($nutritionnistes as $nutritionniste)
                    <div class="col-md-3 col-lg-3">
                        <a href="{{ route('front.nutritionnistes.index')}}">
                            <div class="service-item bg-secondary rounded border border-secondary">
                                <img src="{{asset($nutritionniste->avatar ?? asset('img/default-user.png'))}}"
                                     class="img-fluid rounded-top w-100" style="height: 300px; object-fit: cover;" alt="">
                                <div class="px-4 rounded-bottom">
                                    <div class="service-content bg-primary text-center p-4 rounded">
                                        <h5 class="text-white">{{ $nutritionniste->fullName() }}</h5>
                                        <h6>
                                            <i class="fas fa-map-marker-alt me-2 text-secondary"></i>{{ $nutritionniste->address}}
                                        </h6>
                                        <h3 class="mb-2">
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex align-items-center" style="font-size: 12px;">
                                                    <i class="fa fa-star text-muted"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- nutritionniste End -->

    <!-- Shop Start -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="text-center mx-auto mb-5" style="max-width: 700px;">
                <h1 class="display-4">Nos produits</h1>
                <p>Découvrez notre gamme de produits sains conçus pour nourrir et soutenir votre bien-être.</p>
            </div>
            <div class="row g-4">
                @foreach($produits as $produit)
                    <div class="col-lg-6 col-xl-4">
                        <div class="card h-100 border border-secondary">
                            <div class="row g-0 align-items-center">
                                <div class="col-md-5">
                                    <img src="{{ asset($produit->image) }}" class="card-img img-fluid rounded-start" alt="{{ $produit->name }}">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="{{ route('shopdetails.index', $produit->id) }}" class="text-decoration-none">{{ $produit->name }}</a></h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{ $produit->price }} DT</h6>
                                        <a href="{{ route('ajout.cart', $produit->id) }}" class="btn btn-primary rounded-pill"><i class="fa fa-shopping-bag me-2"></i> Ajouter au panier</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Shop  End -->


    <!-- Fact Start -->
    <div class="container-fluid py-5">
        <div class="container">
            <div class="bg-light p-5 rounded">
                <div class="row g-4 justify-content-center">
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="counter bg-white rounded p-5">
                            <i class="fa fa-users text-secondary"></i>
                            <h4>satisfied customers</h4>
                            <h1>1963</h1>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="counter bg-white rounded p-5">
                            <i class="fa fa-users text-secondary"></i>
                            <h4>quality of service</h4>
                            <h1>99%</h1>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="counter bg-white rounded p-5">
                            <i class="fa fa-users text-secondary"></i>
                            <h4>quality certificates</h4>
                            <h1>33</h1>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="counter bg-white rounded p-5">
                            <i class="fa fa-users text-secondary"></i>
                            <h4>Available Products</h4>
                            <h1>789</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fact Start -->

@endsection
