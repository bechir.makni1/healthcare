
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nutritionniste Approuvé</title>
    <style>
        /* Styles généraux */
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            color: #333333;
        }

        .container {
            width: 100%;
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .header {
            background-color: #4CAF50; /* Vert */
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        .content {
            padding: 20px;
            text-align: left;
            background-color: #f1f1f1;
        }

        .footer {
            text-align: center;
            padding: 10px 0;
            font-size: 12px;
            color: #999999;
            background-color: #f1f1f1;
        }

        h1 {
            font-size: 24px;
            color: #4CAF50; /* Vert */
        }

        p {
            font-size: 16px;
            line-height: 1.5;
        }

        ul {
            padding-left: 20px;
        }

        ul li {
            margin-bottom: 10px;
        }

        .btn {
            display: inline-block;
            background-color: #FFD700; /* Jaune */
            color: #ffffff;
            padding: 10px 20px;
            text-decoration: none;
            border-radius: 5px;
            margin-top: 20px;
        }

        .btn:hover {
            background-color: #FFC107; /* Couleur légèrement plus foncée pour l'effet hover */
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <h1>Rendez-vous accepté</h1>
    </div>
    <div class="content">
        <h1>Bonjour {{ $nutritionist->first_name . ' ' .  $nutritionist->last_name }},</h1>
        <p>Votre compte a été approuvé. Vous pouvez maintenant vous connecter et commencer à utiliser nos services.</p>

    </div>
    <div class="footer">
        <p>&copy; {{ date('Y') }} HealthCare. Tous droits réservés.</p>
    </div>
</div>
</body>
</html>
