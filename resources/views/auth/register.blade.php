@extends('layouts.app')
<style>
    #selected-files {
        max-height: 200px; /* Adjust as needed */
        overflow-y: auto; /* Enable vertical scroll if needed */
    }

</style>
@section('content')

    <!-- Login 4 - Bootstrap Brain Component -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://unpkg.com/bs-brain@2.0.3/components/logins/login-4/assets/css/login-4.css">

    <section class="p-3 p-md-4 p-xl-5">
        <div class="container">
            <div class="card border-light-subtle shadow-sm">
                <div class="row g-0">
                    <!-- Image Section -->
                    <div class="col-md-6">
                        <img class="img-fluid rounded-start w-100 h-100 object-fit-cover" loading="lazy"
                             src="img/login2.png" alt="BootstrapBrain Logo">
                    </div>
                    <!-- Form Section -->
                    <div class="col-md-6">
                        <div class="card-body p-3 p-md-4 p-xl-5">
                            <div class="card">
                                {{--                                <div class="card-header">{{ __('Register') }}</div>--}}
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-5 text-center ">
                                                <h3>Enregistrez-vous </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('post')
                                        <div class="row mb-3">

                                            <div class="col-md-12">
                                                <input id="last_name" type="text"
                                                       class="form-control @error('last_name') is-invalid @enderror"
                                                       name="last_name" value="{{ old('last_name') }}" required
                                                       autocomplete="last_name"
                                                       autofocus placeholder="Votre Nom" >

                                                @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            {{--                                            <label for="first_name"--}}
                                            {{--                                                   class="col-md-4 col-form-label text-md-end">{{ __('Prénom') }}</label>--}}

                                            <div class="col-md-12">
                                                <input id="first_name" type="text"
                                                       class="form-control @error('first_name') is-invalid @enderror"
                                                       name="first_name" value="{{ old('first_name') }}" required
                                                       autocomplete="first_name" autofocus  placeholder="Votre Prénom">

                                                @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <input id="phone" type="text"
                                                       class="form-control @error('phone') is-invalid @enderror"
                                                       name="phone" value="{{ old('phone') }}" required
                                                       autocomplete="phone" autofocus placeholder="Votre numéro de téléphone">

                                                @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <input id="address" type="text"
                                                       class="form-control @error('address') is-invalid @enderror"
                                                       name="address" value="{{ old('address') }}" required
                                                       autocomplete="address" autofocus placeholder="Votre Adresse">

                                                @error('address')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <input id="email" type="email"
                                                       class="form-control @error('email') is-invalid @enderror"
                                                       name="email" value="{{ old('email') }}" required
                                                       autocomplete="email" placeholder="Votre E_mail">

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <input id="password" type="password"
                                                       class="form-control @error('password') is-invalid @enderror"
                                                       name="password" required autocomplete="new-password" placeholder="Votre mot de passe">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <input id="password-confirm" type="password" class="form-control"
                                                       name="password_confirmation" required
                                                       autocomplete="new-password" placeholder="confirme mot de passe ">
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="files"><strong>Tu est un client ou un nutritionniste <span class="text-danger">*</span></strong></label>
                                            <br>
                                            <select name="type" id="role" class="form-control"
                                                    style="margin: 0 auto; display: block;">
                                                <option value="client" selected>Client</option>
                                                <option value="nutritionniste">Nutritionniste</option>
                                            </select>
                                        </div>
                                        <div id="nutritionistform" style="display: none;" class="row mb-3">
                                            <label for="document">Documents</label>
                                            <span class="text-muted">(document(s) doivent être au format PDF)</span>
                                            <input type="file" id="documents" name="documents[]" accept=".pdf" multiple>
                                        </div>
                                        <br/>
                                        <div class="col-12">
                                            <div class="d-grid">
                                                <button class="btn bsb-btn-xl btn-success" type="submit"> {{ __('Register') }}</button>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- Social login buttons -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var typeSelect = document.getElementById('role');
            var nutritionnisteForm = document.getElementById('nutritionistform');

            typeSelect.addEventListener('change', function () {
                if (typeSelect.value === 'nutritionniste') {
                    nutritionnisteForm.style.display = 'block';
                } else {
                    nutritionnisteForm.style.display = 'none';
                }
            });
        });
    </script>

@endsection
