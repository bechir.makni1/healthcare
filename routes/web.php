<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AvailabilityController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\FrontCategoryController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfilClientController;
use App\Http\Controllers\RecetteController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ConseilController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\GestionInformationController;
use App\Http\Controllers\FrontController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('welcome');
// Routes accessibles à tous les utilisateurs
Auth::routes(['reset' => true]);
Route::get('logout', [LoginController::class, 'logout'])->name('admin.logout');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
//MAIL REST PASSWORD
Route::post('password/reset' , [\App\Http\Controllers\Auth\ResetPasswordController::class, 'resetPassword'])->name('password.update');
Route::get('/test-email', function () {
    Mail::raw('This is a test email', function ($message) {
        $message->to('recipient@example.com')
            ->subject('Test Email from Laravel');
    });

    return 'Test email sent.';
});
//

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/nutritionnistes', [FrontController::class, 'nutritionnistes'])->name('front.nutritionnistes.index');
Route::get('/nutritionnistes/search', [FrontController::class, 'searchNutritionnistes'])->name('nutritionnistes.search');
Route::get('/recettes/search', 'App\Http\Controllers\FrontController@search')->name('recette.search');

//category recette
Route::get('/category/{slug}',[App\Http\Controllers\FrontCategoryController::class, 'index'])->name('category.recette');

//route wishlist
Route::get('/wishlist', [WishlistController::class, 'index'])->name('wishlist');
Route::post('/wishlist/add/{id}', [WishlistController::class, 'add'])->name('wishlist.add')->middleware('auth');
Route::delete('/supprimer/item/wishlist/{id}', [WishlistController::class, 'delete'])->name('wishlist.remove')->middleware('auth');

//route rating

Route::post('/products/{product}/rate', [RatingController::class, 'storeProductRating'])->name('products.rate');
Route::post('/nutritionists/rate', [RatingController::class, 'storeNutritionistRating'])->name('nutritionists.rate');

Route::get('/recette', [FrontController::class, 'recettes'])->name('front.recettes.index');
Route::get('/recettes/{id}', [FrontController::class, 'recettedetails'])->name('front.recettedetails.index');
Route::get('/conseils', [FrontController::class, 'conseil'])->name('conseils');
Route::get('/conseils/{id}', [FrontController::class, 'conseildetails'])->name('front.conseildetails.index');
Route::get('/conseils/search', [FrontController::class, 'searchConseils'])->name('conseils.search');

Route::post('/conseils/{id}/comment', [App\Http\Controllers\CommentController::class, 'createConseil'])->name('comment.create.conseil');
Route::get('/shop',[FrontController::class,'shop'])->name('shop');
Route::get('/shop/search', [FrontController::class, 'shop'])->name('shop.search');
Route::get('/shop/{id}',[FrontController::class,'shopdetails'])->name('shopdetails.index');

Route::post('ajout/commande',[CommandeController::class,'store'])->name('commande.store')->middleware('auth');
Route::get('caisse',[CommandeController::class,'index'])->name('front.caisse.index')->middleware('auth');
//route cart
Route::get('/cart',[CartController::class,'index'])->name('cart.index')->middleware('auth');
Route::post('/ajout-cart/{id}', [CartController::class, 'ajout'])->name('ajout.cart');
Route::get('/supprimer/item/cart/{id}',[CartController::class,'delete'])->name('supprimer.cart')->middleware('auth');
Route::post('/edit/{id}',[CartController::class,'update'])->name('modifier.cart')->middleware('auth');


Route::post('/appointment', [AppointmentController::class, 'create'])->name('appointment.store');
Route::resource('appointments', AppointmentController::class);
Route::get('/api/appointments', [AppointmentController::class, 'apiAppointments'])->name('api.appointments');
Route::get('/appointments/list', [AppointmentController::class, 'listAppointments'])->name('appointments.list');
Route::resource('availabilities', AvailabilityController::class);

Route::put('/nutritionnistes/{nutritionniste}/approve', [\App\Http\Controllers\NutritionnisteController::class, 'approve'])->name('nutritionnistes.approve');
Route::patch('/nutritionniste/approve', [\App\Http\Controllers\NutritionnisteController::class, 'nutritionniste'])->name('approve.nutritionniste');
Route::get('/apropos', [App\Http\Controllers\FrontController::class, 'apropos'])->name('apropos');
Route::post('/recettes/{id}/comment', [App\Http\Controllers\CommentController::class, 'create'])->name('comment.create');

Route::get('profile', [\App\Http\Controllers\ProfileController::class, 'edit'])->name('admin.profile.edit');
Route::patch('profile', [\App\Http\Controllers\ProfileController::class, 'update'])->name('admin.profile.update');
Route::patch('modifier/password', [\App\Http\Controllers\Auth\PasswordController::class, 'update'])->name('password.update');



    Route::get('user/account', [ProfilClientController::class, 'edit'])->name('client.account.edit');
    Route::patch('user/account/edit', [ProfilClientController::class, 'update'])->name('client.account.update');
    Route::patch('account/password', [ProfilClientController::class, 'updatePassword'])->name('password.update.account');

    //Route access admin
Route::prefix('admin')->middleware('auth','admin')->group(function () {
   //Route settings
    Route::get('/paramettre', [SettingController::class, 'index'])->name('settings.index');
    Route::put('/paramettre/{settings}', [SettingController::class, 'update'])->name('settings.update');
//Route auteurs
    Route::resource('nutritionniste', App\Http\Controllers\NutritionnisteController::class)->shallow();
    Route::resource('client', App\Http\Controllers\clientController::class)->shallow();
//Route categories
    Route::resource('/category', CategoryController::class)->except(['show']);
//Route produits
    Route::resource('/produit', ProductController::class);
    //Route commande-cote-admin
    Route::get('/commandes',[\App\Http\Controllers\CommandeController::class, 'Admin'])->name('commande.admin');
    Route::delete('/admin/commandes/{id}', [CommandeController::class, 'destroy'])->name('admin.commandes.destroy');
    Route::get('/commande/voir/{id}',[\App\Http\Controllers\CommandeController::class, 'show'])->name('commande.voir');
    Route::get('commandes/confirm/{id}', [CommandeController::class, 'confirm'])->name('admin.commandes.confirm');
    Route::get('commandes/annuler/{id}', [CommandeController::class, 'annuler'])->name('admin.commandes.annuler');

});

//Route admin-nutritionniste
Route::prefix('dashboard')->middleware(['auth', 'CheckRole:' . User::TYPE_SUPER_ADMIN . ',' . User::TYPE_NUTRITIONNISTE])->group(function () {
    Route::get('/acceuil', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::resource('conseil', App\Http\Controllers\ConseilController::class);
    // Route::resource('client', ClientController::class)->except(['show']);
    Route::resource('/recette', RecetteController::class);
//Route commentaires
    Route::get('/comment', [\App\Http\Controllers\CommentController::class,'index'])->name('comment.index');
    Route::put('/comment/update/{comment}',[CommentController::class,'update'])->name('comment.update');
    Route::put('/comment/debloque/{id}',[CommentController::class,'debloquer'])->name('comment.debloquer');

});

//Route Nuutritionniste
Route::prefix('nutritionniste')->middleware('auth','Nutritionniste')->group(function () {
    Route::resource('/compte', GestionInformationController::class);

});
// Routes pour les utilisateurs invités (non connectés)
Route::middleware('guest')->group(function () {

    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::get('password/reset', [\App\Http\Controllers\Auth\ResetPasswordController::class, 'showReset'])->name('password.request');
    Route::post('login', [LoginController::class, 'login'])->name('admin.login.submit');
});
