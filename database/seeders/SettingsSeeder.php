<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Setting::create([
            'web_site_name' => 'Health Care',
            'address' => 'Tunisia',
            'phone' => '+2162855669',
            'email' => 'Health@gmail.com',
            'about' => 'Un site de santé (healthcare) est une plateforme en ligne conçue pour fournir des informations, des services et des ressources liées à la santé et au bien-être. Ces sites visent à aider les utilisateurs à prendre des décisions éclairées concernant leur santé, à accéder à des services médicaux et à améliorer leur qualité de vie.',
        ]);
    }
}
