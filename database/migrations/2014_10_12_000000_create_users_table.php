<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', [
                User::TYPE_SUPER_ADMIN,
                User::TYPE_CLIENT,
                User::TYPE_NUTRITIONNISTE,
            ])->default(User::TYPE_CLIENT);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('avatar')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->string('password');
            $table->string('address')->nullable();
            $table->boolean('isActive')->default(true);
            $table->boolean('approved')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};


