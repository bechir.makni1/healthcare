<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Default Mailer
    |--------------------------------------------------------------------------
    |
    | Cette option contrôle le mailer par défaut utilisé pour envoyer tout
    | message électronique envoyé par votre application. Des mailers alternatifs
    | peuvent être configurés et utilisés selon les besoins ; cependant, ce mailer
    | sera utilisé par défaut.
    |
    */

    'default' => env('MAIL_MAILER', 'smtp'),

    /*
    |--------------------------------------------------------------------------
    | Configurations de Mailer
    |--------------------------------------------------------------------------
    |
    | Ici, vous pouvez configurer tous les mailers utilisés par votre application
    | ainsi que leurs paramètres respectifs. Plusieurs exemples ont été configurés
    | pour vous et vous êtes libre d'ajouter les vôtres selon les besoins de votre
    | application.
    |
    | Laravel supporte une variété de drivers de "transport" de mail à utiliser
    | lors de l'envoi d'un e-mail. Vous indiquerez lequel vous utilisez pour vos
    | mailers ci-dessous. Vous êtes libre d'ajouter des mailers supplémentaires
    | selon vos besoins.
    |
    | Supportés : "smtp", "sendmail", "mailgun", "ses", "ses-v2",
    |             "postmark", "log", "array", "failover", "roundrobin"
    |
    */

    'mailers' => [
        'smtp' => [
            'transport' => 'smtp',
            'url' => env('MAIL_URL'),
            'host' => env('MAIL_HOST', 'smtp-relay.sendinblue.com'),
            'port' => env('MAIL_PORT', 587),
            'encryption' => env('MAIL_ENCRYPTION', 'tls'),
            'username' => env('MAIL_USERNAME'),
            'password' => env('MAIL_PASSWORD'),
            'timeout' => null,
            'local_domain' => env('MAIL_EHLO_DOMAIN'),
        ],

        'ses' => [
            'transport' => 'ses',
        ],

        'postmark' => [
            'transport' => 'postmark',
            // 'message_stream_id' => null,
            // 'client' => [
            //     'timeout' => 5,
            // ],
        ],

        'mailgun' => [
            'transport' => 'mailgun',
            // 'client' => [
            //     'timeout' => 5,
            // ],
        ],

        'sendmail' => [
            'transport' => 'sendmail',
            'path' => env('MAIL_SENDMAIL_PATH', '/usr/sbin/sendmail -bs -i'),
        ],

        'log' => [
            'transport' => 'log',
            'channel' => env('MAIL_LOG_CHANNEL'),
        ],

        'array' => [
            'transport' => 'array',
        ],

        'failover' => [
            'transport' => 'failover',
            'mailers' => [
                'smtp',
                'log',
            ],
        ],

        'roundrobin' => [
            'transport' => 'roundrobin',
            'mailers' => [
                'ses',
                'postmark',
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Adresse "From" Globale
    |--------------------------------------------------------------------------
    |
    | Vous pouvez souhaiter que tous les e-mails envoyés par votre application
    | soient envoyés de la même adresse. Ici, vous pouvez spécifier un nom et
    | une adresse utilisés globalement pour tous les e-mails envoyés par votre
    | application.
    |
    */

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'your_email@example.com'),
        'name' => env('MAIL_FROM_NAME', 'Your App Name'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Paramètres de Mail Markdown
    |--------------------------------------------------------------------------
    |
    | Si vous utilisez le rendu d'e-mails basé sur Markdown, vous pouvez
    | configurer vos thèmes et chemins de composants ici, vous permettant de
    | personnaliser le design des e-mails. Ou, vous pouvez simplement utiliser
    | les valeurs par défaut de Laravel !
    |
    */

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];

