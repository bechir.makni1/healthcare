<?php

namespace App\Livewire;

use App\Models\Appointment;
use Livewire\Component;

class Calendar extends Component
{
    public $appointments = [];


    public function mount()
    {
        $this->appointments = Appointment::with('user')
            ->where('nutritionist_id', auth()->user()->id)
            ->where('status', 'confirmed')
            ->get();

    }

    public function render()
    {

        return view('livewire.calendar', [
            'events' => $this->appointments->map(function($appointment) {

                return [
                    'title' => $appointment->user->fullName(),
                    'start' => $appointment->date . 'T' . $appointment->start_at,
                    'end' => $appointment->date . 'T' . $appointment->end_at,
                ];
            })->toArray()
        ]);
    }
}
