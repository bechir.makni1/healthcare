<?php

namespace App\Livewire;

use App\Mail\AppointmentAccepted;
use App\Mail\AppointmentRefused;
use App\Models\Appointment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class NutritionistAppointments extends Component
{
    public $appointments;

    public function mount()
    {
        $this->appointments = Appointment::with('user')
            ->where('nutritionist_id', Auth::id())
            ->where('status', 'pending')
            ->get();
    }

    public function acceptAppointment($appointmentId)
    {
        $appointment = Appointment::find($appointmentId);
        if ($appointment && $appointment->nutritionist_id == Auth::id()) {

            if ($appointment->update(['status' => 'confirmed'])){
                Mail::to($appointment->user->email)->send(new AppointmentAccepted($appointment));
            }

            $this->appointments = Appointment::where('nutritionist_id', Auth::id())->where('status', 'pending')->get();
        }
    }
    public function refuserAppointment($appointmentId)
    {
        $appointment = Appointment::find($appointmentId);
        if ($appointment && $appointment->nutritionist_id == Auth::id()) {
           if ($appointment->update(['status' => 'cancelled'])){
               Mail::to($appointment->user->email)->send(new AppointmentRefused($appointment));
           }
            $this->appointments = Appointment::where('nutritionist_id', Auth::id())->where('status', 'pending')->get();
        }
    }

    public function render()
    {
        return view('livewire.nutritionist-appointments');
    }
}
