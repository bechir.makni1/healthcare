<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use HasFactory,HasSlug;
    protected $fillable = ['name','slug','details','description','image','price','livraison','user_id'];


    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
    public function imageUrl():string
    {
        return storage::url($this->image);
    }

    public function user():BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
    public function commande()
    {
        return $this->belongsToMany(Commande::class, 'order_product')->withPivot('quantity')->withTimestamps();
    }
    public function ratings()
    {
        return $this->morphMany(Rating::class, 'rateable');
    }
}
