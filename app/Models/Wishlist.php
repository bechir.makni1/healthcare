<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'produit_id'];

    public function produits()
    {
        return $this->belongsTo(Product::class,'produit_id', 'id');
    }}
