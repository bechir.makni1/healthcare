<?php

namespace App\Models;

use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;


class Recettes extends Model
{
    use HasFactory,HasSlug,Taggable;
    protected $fillable = ['title','slug','image','description','ingrédient','isActive','isComment','category_id','user_id'];


    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function imageUrl():string
    {
      return storage::url($this->image);
    }


    public function category():BelongsTo
    {
      return $this->belongsTo(Category::class, 'category_id','id');
    }
    public function user():BelongsTo
    {
      return $this->belongsTo(User::class, 'user_id','id');
    }
    public function comment()
    {
        return $this->hasMany(Comment::class ,'recettes_id','id')
            ->where('isActive', 1);
    }
}

