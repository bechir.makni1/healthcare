<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Conseil extends Model
{
    use HasFactory;
    protected $fillable = ['image','titre','contenu','isComment','isActive','isSharable' ,'user_id','views' ];

    public function user()
    {
        return $this->belongsTo(User::class ,'user_id' , 'id');
    }

    public function imageUrl()
    {
        if ($this->image) {
            return Storage::url($this->image);
        }
        return null;
    }
    public function comment()
    {
        return $this->hasMany(Comment::class,'conseils_id','id')
            ->where('isActive',1);
    }

}
