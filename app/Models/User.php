<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    public const TYPE_SUPER_ADMIN = 'super admin';
    public const TYPE_CLIENT = 'client';
    public const TYPE_NUTRITIONNISTE = 'nutritionniste';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'first_name',
        'last_name',
        'avatar',
        'birth_date',
        'phone',
        'email',
        'password',
        'address',
        'isActive',
        'approved',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name ;
    }
    public function isApproved()
    {
      return  $this->approved === 1 ;
    }
    public function getAvatarUrlAttribute(){
        return $this->avatar ? asset('storage/avatars/' . $this->avatar) : asset('img/default-user.png');
    }

    public function conseil()
    {
        return $this->hasMany(Conseil::class);

    }
    public function attachments()
    {
        return $this->hasMany(Attachment::class);

    }
    public function clientAppointments()
    {
        return $this->hasMany(Appointment::class, 'client_id');
    }

    public function nutritionistAppointments()
    {
        return $this->hasMany(Appointment::class, 'nutritionist_id');
    }
    public function gestionInformations()
    {
        return $this->hasOne(GestionInformation::class,'user_id');
    }
    public function ratings()
    {
        return $this->morphMany(Rating::class, 'rateable');
    }
    public function averageRating()
    {
        return $this->ratings()->avg('rating');
    }
}
