<?php

namespace App\Models;

use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{

    use HasFactory;

    protected $fillable = ['content','isActive','user_id','conseils_id','recettes_id'];


    public function user():BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function recette():BelongsTo
    {
        return $this->belongsTo(Recettes::class, 'recettes_id','id');
    }

    public function conseil():BelongsTo
    {
        return $this->belongsTo(Conseil::class, 'conseils_id','id');
    }
}
