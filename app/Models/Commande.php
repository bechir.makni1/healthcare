<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class Commande extends Model
{
    use HasFactory;
    protected $fillable = ['addresse','code_postal','pays','ville','telephone','note','user_id','total'];


    public function user():BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product')->withPivot('quantity')->withTimestamps();
    }



}

