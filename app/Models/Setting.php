<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Setting extends Model
{
    use HasFactory;
    protected $fillable=['web_site_name','logo','address','phone','email','about'];

    public function imageUrl():string
    {
        return storage::url($this->image);
    }

}
