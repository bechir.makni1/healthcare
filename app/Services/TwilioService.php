<?php
namespace App\Services;

use App\Models\Commande;
use Twilio\Rest\Client;
use App\Models\User;
use Twilio\Http\CurlClient;
class TwilioService
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client(env('TWILIO_SID'), env('TWILIO_TOKEN'));
    }

    // Méthode pour envoyer un SMS à un utilisateur spécifique
    public function sendSmsToUser(Commande $commande, $message)
    {
        $curlOptions = [ CURLOPT_SSL_VERIFYHOST => false, CURLOPT_SSL_VERIFYPEER => false];
        $this->client->setHttpClient(new CurlClient($curlOptions));

        // Récupérer le numéro de téléphone de l'utilisateur
        $to = "+216".$commande->telephone;

        // Envoie un message SMS en utilisant l'API Twilio
        return $this->client->messages->create(
            $to,
            [
                'from' => env('TWILIO_FROM'),
                'body' => $message
            ]
        );
    }
}
