<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access-admin', function ($user) {
            return $user->type === User::TYPE_SUPER_ADMIN;
        });
        Gate::define('access-nutritionist', function ($user) {
            return $user->type === User::TYPE_NUTRITIONNISTE;
        });
    }
}
