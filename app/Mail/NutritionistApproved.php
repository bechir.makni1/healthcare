<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class NutritionistApproved extends Mailable
{
    use Queueable, SerializesModels;
    public $nutritionist;

    /**
     * Create a new message instance.
     */
    public function __construct(User $nutritionist)
    {
        $this->nutritionist = $nutritionist;
    }

    public function build()
    {
        return $this->subject('Votre compte Nutritionniste a été approuvé')
            ->view('emails.nutritionist_approved');
    }
}
