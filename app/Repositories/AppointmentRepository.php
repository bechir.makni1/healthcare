<?php

namespace App\Repositories;
use App\Models\Appointment;
use App\Models\Comment;
class AppointmentRepository extends BaseRepository
{
    protected $model;

    public function __construct(Appointment $appointment)
    {
        $this->model = $appointment;
    }

    public function model(): string
    {
        return Appointment::class;
    }
    protected $fieldSearchable = [
        'first_name',
        'last_name',
    ];
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

}
