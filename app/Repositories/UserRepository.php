<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function model(): string
    {
        return User::class;
    }
    protected $fieldSearchable = [
        'first_name',
        'last_name',
    ];
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }
}
