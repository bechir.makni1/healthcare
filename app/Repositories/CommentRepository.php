<?php

namespace App\Repositories;
use App\Models\Comment;
class CommentRepository extends BaseRepository
{
    protected $model;

    public function __construct(Comment $comment)
    {
        $this->model = $comment;
    }

    public function model(): string
    {
        return Comment::class;
    }
    protected $fieldSearchable = [
        'first_name',
        'last_name',
    ];
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

}
