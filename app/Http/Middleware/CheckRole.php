<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  mixed  ...$types
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$types)
    {
        $user = Auth::user();

        if ($user && $this->hasAnyRole($user, $types) && $user->approved) {
            return $next($request);
        }

        abort(403, 'vous n êtes pas encore approuvé');
    }


    /**
     * Check if the user has any of the given roles.
     *
     * @param  \App\Models\User  $user
     * @param  array  $types
     * @return bool
     */
    protected function hasAnyRole($user, $types)
    {
        return in_array($user->type, $types);
    }
}
