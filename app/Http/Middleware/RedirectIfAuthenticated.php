<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if (Auth::user()->type ==  User::TYPE_SUPER_ADMIN){
                    return redirect(RouteServiceProvider::HOME);
                }elseif (Auth::user()->type ==  User::TYPE_NUTRITIONNISTE){
                    return redirect(RouteServiceProvider::NUTRITIONNISTE);
                }else{
                    return redirect()->to('/');
                }

            }
        }

        return $next($request);
    }
}
