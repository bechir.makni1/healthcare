<?php

namespace App\Http\Controllers;

use App\Models\Recettes;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;
use App\Models\Conseil;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class ConseilController extends Controller
{
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function index()
    {
        if (Auth::check() && Auth::user()->type === User::TYPE_SUPER_ADMIN) {
            $conseils = Conseil::all()->sortByDesc('created_at');
        } else {
            $conseils = Auth::user()->conseil->sortByDesc('created_at');
        }
        return view('admin.conseil.index', compact('conseils'));
    }

    public function create()
    {
        return view('admin.conseil.create');
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => ['image', 'nullable', 'mimes:jpg,png,jpeg', 'max:2048'],
            'titre' => ['string', 'max:255'],
            'contenu' => ['string', 'required'],
        ]);


        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $file_name = date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images'), $file_name);
            $imagePath = 'images/' . $file_name;
        }

        $conseil = Conseil::create([
            'titre' => $validatedData['titre'],
            'contenu' => $validatedData['contenu'],
            'image' => $imagePath,
            'user_id' => Auth::user()->id,
        ]);

        return redirect()->route('conseil.index')->with('success', 'Conseil ajouté avec succès');
    }


    public function edit($id)
    {
        $conseil = Conseil::findOrFail($id);
        return view('admin.conseil.edit', compact('conseil'));
    }

    public function update(Request $request, $id)
    {
        $conseil = Conseil::findOrFail($id); // Récupérer le conseil à mettre à jour

        $validatedData = $request->validate([
            'image' => ['image', 'nullable', 'mimes:jpg,png,jpeg', 'max:2048'],
            'titre' => ['string', 'max:255'],
            'contenu' => ['string', 'required'],
            'isComment' => ['required', 'boolean'],
            'isActive' => ['required', 'boolean'],
            'isSharable' => ['required', 'boolean'],
        ]);

        // Récupérer l'image actuelle du conseil
        $image = $conseil->image;

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            // Store new avatar
            $file = $request->file('image');
            $file_name = date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images'), $file_name);
            $imagePath = 'images/' . $file_name;
        }

        // Mettre à jour les données du conseil avec les nouvelles données validées
        $conseil->update([
            'titre' => $validatedData['titre'],
            'contenu' => $validatedData['contenu'],
            'isActive' => $validatedData['isActive'],
            'isSharable' => $validatedData['isSharable'],
            'isComment' => $validatedData['isComment'],
            'image' => $imagePath ?? $conseil->image,
        ]);

        return redirect()->route('conseil.index')->with('success', 'Conseil mis à jour avec succès');
    }

    public function show(Conseil $conseil)
    {
        return view('admin.conseil.show', ['conseil' => $conseil]);

    }

    public function destroy($id)
    {
        $conseil = Conseil::findOrFail($id);
        $conseil->delete();

        return redirect()->route('conseil.index')->with('success', 'Conseil supprimé avec succès');
    }

    public function comment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
        ]);

        $conseil = Conseil::findOrFail($id);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Il y a des erreurs de validation.');
        }

        // Si la validation réussit, continuez à créer le commentaire
        $data = $validator->validated();

        $data = [
            'conseils_id' => $id,
            'content' => $request->get('content'),
            'user_id' => Auth::id()
        ];
        $comments = $this->commentRepository->create($data);

        // Passez la recette, la catégorie et le message de succès à la vue
        return redirect()->back()->with('success', 'Commentaire ajouté avec succès!')->with(compact('conseil', 'comments'));
    }
}






