<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\ResetPasswordController;
use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlistItems = Wishlist::where('user_id', Auth::id())->with('produits')->get();
        return view('front.wishlist',compact('wishlistItems'));
    }
    public function add(Request $request, $id)
    {
        if (Auth::check()) {
            $produit_id = Product::findOrFail($id);
            $wish = new Wishlist();
            $wish->produit_id = $produit_id->id;
            $wish->user_id = Auth::id();
            $wish->save();
            return redirect()->back()->with('status', 'Product added to favoris!');

        } else {
            return redirect()->back()->with('status', 'connectez-vous!');
        }
    }
    public function delete($id)
    {
        $wishlistItem = Wishlist::findOrFail($id);
        $wishlistItem->delete();
        return redirect()->route('wishlist')->with('status', 'Produit supprimée avec succès');
    }
}
