<?php
namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Conseil;
use Illuminate\Http\Request;
use App\Models\Recettes;
use App\Repositories\CommentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    protected $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function create(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
        ]);

        // Vérifiez s'il y a des erreurs de validation
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Il y a des erreurs de validation.');
        }

        $recette = Recettes::findOrFail($id);

        // Si la validation réussit, continuez à créer le commentaire
        $data = $validator->validated();
        $data['recettes_id'] = $id;
        $data['user_id'] = Auth::id();
        $data['conseils_id'] = null; // Ensure conseils_id is null

        $comment = $this->commentRepository->create($data);

        // Passez la recette et le message de succès à la vue
        return redirect()->back()->with('status', 'Commentaire ajouté avec succès!')->with(compact('recette', 'comment'));
    }

    public function createConseil(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
        ]);

        // Vérifiez s'il y a des erreurs de validation
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Il y a des erreurs de validation.');
        }

        $conseil = Conseil::findOrFail($id);

        // Si la validation réussit, continuez à créer le commentaire
        $data = $validator->validated();
        $data['conseils_id'] = $id;
        $data['user_id'] = Auth::id();
        $data['recettes_id'] = null; // Ensure recettes_id is null

        $comment = $this->commentRepository->create($data);

        // Passez le conseil et le message de succès à la vue
        return redirect()->back()->with('status', 'Commentaire ajouté avec succès!')->with(compact('conseil', 'comment'));
    }

    public function index()
    {
        $user = Auth::user();
        $comments = Comment::whereHas('recette', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->orWhereHas('conseil', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->get();

        return view('admin.comment', compact('comments'));
    }

    public function update(Comment $comment)
    {
        $comment->isActive = 0;
        $comment->save();
        return back()->with('success', 'Commentaire bloqué avec succès');
    }

    public function debloquer(int $id)
    {
        $comment = Comment::where('id', $id)->first();
        $comment->isActive = 1;
        $comment->save();
        return back()->with('success', 'Commentaire débloqué avec succès');
    }
}
