<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        $user = Auth::user();
        $compte = $user->gestionInformations()->first() ?? null ;

        return view('admin.profile.edit', [
            'user' => $request->user(), 'compte' => $compte
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(Request $request): RedirectResponse
    {
        try {
            $user = Auth::user();

            if ($user) {
                // Valider les données de la requête
                $validatedData = $request->validate([
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
                    'avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Ajoutez les règles de validation pour l'image si nécessaire
                ]);

                // Mettre à jour les attributs de l'utilisateur
                $user->fill([
                    'first_name' => $validatedData['first_name'],
                    'last_name' => $validatedData['last_name'],
                    'email' => $validatedData['email'],
                ]);

                // Gérer le téléchargement de l'image
                if ($request->hasFile('avatar')) {
                    $oldImagePath = $user->avatar;

                    try {
                        if (!empty($oldImagePath) && file_exists(public_path($oldImagePath))) {
                            unlink(public_path($oldImagePath));
                        }
                    } catch (\Exception $exception) {
                        return redirect()->back()->withErrors(['avatar' => 'Image not found']);
                    }

                    $ext = $request->file('avatar')->extension();
                    $file_name = date('YmdHis') . '.' . $ext;
                    $request->file('avatar')->move(public_path('avatars'), $file_name);
                    $user->avatar = 'avatars/' . $file_name;
                }


                // Sauvegarder les changements de l'utilisateur
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->save();

                return redirect()->route('admin.profile.edit')->with('status', 'Profile updated successfully');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['user' => 'User not found']);
        };
        return Redirect::route('profile.edit')->with('status', 'Profile modifie avec succes');

    }


    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->to('/');
    }
}
