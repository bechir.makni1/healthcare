<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    public function index()
    {
        return view('admin.setting.index',['settings' => Setting::where('id', 1)->first()]
        );
    }
    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);
        $validatedData = $request->validate([
            'web_site_name' => 'required|string|max:255',
            'logo' => 'nullable|image|max:2048',
            'address' => 'nullable|string',
            'phone' => 'nullable|string',
            'email' => 'nullable|email',
            'about' => 'nullable|string',
        ]);

        $logoPath = $setting->logo;

        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            if ($logoPath && Storage::disk('public')->exists($logoPath)) {
                Storage::disk('public')->delete($logoPath);
            }
            $logoPath = $request->file('logo')->store('logos', 'public');
        }

        $setting->update([
            'web_site_name' => $validatedData['web_site_name'],
            'logo' => $logoPath,
            'address' => $validatedData['address'],
            'phone' => $validatedData['phone'],
            'email' => $validatedData['email'],
            'about' => $validatedData['about'],
        ]);

        return redirect()->route('settings.index')->with('success', 'Paramètres mis à jour avec succès');
    }

}
