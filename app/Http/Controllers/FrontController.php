<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Rating;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Recettes;
use App\Models\Conseil;
use App\Models\Comment;
use App\Models\Category;
use App\Http\Controllers\CommentController;
use Illuminate\Support\Facades\Auth;


class FrontController extends Controller
{
    public function nutritionnistes(Request $request)
    {
        // Fetch nutritionists with their related gestionInformations
        $nutritionnistes = User::with('gestionInformations')
            ->where('type', 'nutritionniste')->where('approved',true)
            ->get();

        // Calculate ratings and average ratings for each nutritionist
        $nutritionnistes->each(function ($nutritionniste) {
            $ratings = Rating::where('rateable_id', $nutritionniste->id)
                ->where('rateable_type', 'App\Models\User') // Adjust 'App\Models\User' to your actual user model namespace
                ->get();

            $rating_sum = $ratings->sum('rating');
            $rating_count = $ratings->count();

            // Calculate average rating for this nutritionist
            $nutritionniste->average_rating = $rating_count > 0 ? $rating_sum / $rating_count : 0;
            $nutritionniste->ratings = $ratings; // Optionally, you can also attach ratings to each nutritionist
        });

        return view('front.nutritionnistes.index', compact('nutritionnistes'));
    }


    public function searchNutritionnistes(Request $request)
    {
        $query = $request->input('query');

        $nutritionnistes = User::query()
            ->with('gestionInformations')
            ->where('type', 'nutritionniste')
            ->where(function ($q) use ($query) {
                $q->where('first_name', 'like', "%$query%")
                    ->orWhere('last_name', 'like', "%$query%")
                    ->orWhere('email', 'like', "%$query%");
            })
            ->get();

        return view('front.nutritionnistes.index', compact('nutritionnistes'));
    }
    public function recettes()
    {
        $perPage = 9;
        $recettes = Recettes::where('isActive', true)->paginate($perPage); // Correct usage of paginate
        $categories = Category::all();

        return view('front.recettes.index', compact('recettes', 'categories'));
    }

    public function search(Request $request)
    {
        $query = $request->input('query');

        $recettes = Recettes::where('title', 'LIKE', "%$query%")
            ->orderBy('created_at', 'desc')
            ->get();

        $categories = Category::all(); // Si vous avez besoin des catégories dans la vue de résultats de recherche

        return view('front.recettes.index', compact('recettes', 'categories'));
    }
    public function recettedetails($id)
    {
        $recettes = Recettes::find($id);
        $categories = $recettes->category; // Utilise la relation pour récupérer la catégorie associée
        $comments = Comment::where('recettes_id', $id)->where('isActive',1)->get();
        return view('front.recettedetails.index', compact('recettes', 'comments', 'categories'));
    }


    public function conseil()
    {
        $perPage = 9;

        $conseils = Conseil::paginate($perPage);

        foreach ($conseils as $conseil) {
            $new_views = $conseil->views + 1;
            $conseil->views = $new_views;
            $conseil->save();
        }
        return view('front.conseils.index', compact('conseils'));
    }
    public function searchConseils(Request $request)
    {
        $query = $request->input('query');

        // Perform your search logic, for example:
        $conseils = Conseil::where('titre', 'like', '%'.$query.'%')->get();

        // Pass the results to the view
        return view('front.conseils.index', compact('conseils'));
    }

    public function conseildetails($id)
    {
        $conseils = Conseil::find($id);
        $comments = Comment::where('conseils_id', $id)->where('isActive',1)->get();
        return view('front.conseildetails.index', compact('conseils', 'comments'));
    }

    public function apropos()
    {

        return view('front.apropos.index');
    }

    public function shop(Request $request)
    {
        $query = $request->input('query');
        $perPage = 9; // Nombre d'éléments par page

        if ($query) {
            $produits = Product::where('name', 'LIKE', "%$query%")
                ->orWhere('details', 'LIKE', "%$query%")
                ->paginate($perPage);
        } else {
            $produits = Product::paginate($perPage);
        }

        return view('front.shop.index', compact('produits'));
    }


    public function shopdetails($id)
    {
        $produit = Product::findOrFail($id);

        // Assuming the 'rateable_type' for products is 'Product'
        $ratings = Rating::where('rateable_id', $produit->id)
            ->get();
        $rating_user=Rating::where('rateable_id', $produit->id)->where('user_id', Auth::id())->first();

        $rating_sum = $ratings->sum('rating');
        $rating_count = $ratings->count();

        // Avoid division by zero
        $rating_value = $rating_count > 0 ? $rating_sum / $rating_count : 0;

        return view('front.shopdetails.index', compact('produit', 'ratings', 'rating_value','rating_user'));
    }



}

