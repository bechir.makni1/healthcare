<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Mail\NutritionistApproved;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class NutritionnisteController extends Controller
{
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $nutritionnistes = User::all()->where('type', User::TYPE_NUTRITIONNISTE)->sortByDesc('created_at');

        return view('admin.nutritionniste.index', compact('nutritionnistes'));

    }

    public function show($id)
    {
        $nutritionniste = $this->userRepository->find($id);

        /*  if (empty($nutritionniste)) {
              Flash::error('nutritionniste not found');

              return redirect(route('nutritionniste.index'));
          }*/

        return view('admin.nutritionniste.show')->with('nutritionniste', $nutritionniste);
    }

    public function create()
    {
        return view('admin.nutritionniste.create');
    }

    public function store(CreateUserRequest $request)
    {
        $data = $request->all();

        if ($request->hasFile('avatar')) {

            $file = $request->file('avatar');
            $file_name = date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('avatars'), $file_name);
            $imagePath = 'avatars/' . $file_name;
        }
        if ($imagePath){
            $data = array_merge($data, ['avatar' => $imagePath]);
        }
        $this->userRepository->create($data);

        return redirect()->route('nutritionniste.index')->with('success', 'Nutritionniste ajouté avec succès');
    }

    public function edit($id)
    {
        $nutritionniste = $this->userRepository->find($id);

        return view('admin.nutritionniste.edit', compact('nutritionniste'));
    }


    public function update(UpdateUserRequest $request, $id)
    {
        $data = $request->all();

        $nutritionniste = $this->userRepository->find($id);

        $this->userRepository->update($data, $nutritionniste->id);

        return redirect()->route('nutritionniste.index')->with('success', 'Nutritionniste modifié avec succès');
    }

    public function destroy($id)
    {
        $nutritionniste = $this->userRepository->find($id);

        if (empty($nutritionniste)) {
            return redirect()->route('nutritionniste.index')->with('Nutritionniste not found');
        }

        if ($this->userRepository->delete($nutritionniste->id))
            return redirect()->route('nutritionniste.index')->with('success', 'Nutritionniste supprimé avec succès');

    }

    public function approve(User $nutritionniste)
    {
        $this->userRepository->update(['approved'=> true], $nutritionniste->id);

        Mail::to($nutritionniste->email)->send(new NutritionistApproved($nutritionniste));

        return redirect()->route('nutritionniste.index')->with('success', 'Nutritionniste approuvé avec succès');
    }

}
