<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
class CartController extends Controller
{
    public function index()
    {
        return view('front.cart.index');
    }
    public function ajout(Request $request,$id)
    {
        $produit = Product::findOrFail($id);
        $quantity = $request->input('quantity');


        $cartItems = session()->get('cartItems', []);

            if (isset($cartItems[$id])) {
                $cartItems[$id]['quantity']++;
            } else {
                $cartItems[$id] = [
                    "image" => $produit->image,
                    "name" => $produit->name,
                    "description" => $produit->description,
                    "details" => $produit->details,
                    "price" => $produit->price,
                    "livraison" => $produit->livraison,
                    "quantity" => $quantity
                ];
            }

            session()->put('cartItems', $cartItems);
            return redirect()->back()->with('status', 'Product added to cart!');

    }
    public function delete(Request $request)
    {
        if($request->id) {
            $cartItems = session()->get('cartItems');

            if(isset($cartItems[$request->id])) {
                unset($cartItems[$request->id]);
                session()->put('cartItems', $cartItems);
            }

            return redirect()->back()->with('status', 'Product deleted successfully');
        }
    }
    public function update(Request $request)
    {
        if($request->id && $request->quantity){
            $cartItems = session()->get('cartItems');
            $cartItems[$request->id]["quantity"] = $request->quantity;
            session()->put('cartItems', $cartItems);
        }

        return redirect()->back()->with('status', 'Product added to cart!');
    }
    public function show()
    {
        // Fetch cart items from session
        $cartItems = session()->get('cartItems', []);
        $total = array_sum(array_map(fn($item) => $item['price'] * $item['quantity'], $cartItems));

        return view('front.caisse.index', compact('cartItems', 'total'));
    }
}
