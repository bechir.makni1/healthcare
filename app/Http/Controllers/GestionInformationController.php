<?php

namespace App\Http\Controllers;

use App\Models\GestionInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GestionInformationController extends Controller
{
    public function index()
    {
        $comptes=GestionInformation::all();
        return view('admin.compte.index',compact('comptes'));
    } public function create()
    {
        return view('admin.compte.create');
    }
    public function store(Request $request)
    {
                // Validate request
                $validatedData = $request->validate([
                    'bio' => 'required|string',
                    'horaires' => 'required|string|max:255',
                    'specialite' => 'required|string|max:255',
                    'langue' => 'required|string|max:255',
                    'diplome' => 'required|string|max:255',
                    'experience' => 'required|string',

                ]);

                // Create new GestionInformation instance
                GestionInformation::create([
                    'bio' => $validatedData['bio'],
                    'horaires' => $validatedData['horaires'],
                    'specialite' => $validatedData['specialite'],
                    'langue' => $validatedData['langue'],
                    'diplome' => $validatedData['diplome'],
                    'experience' => $validatedData['experience'],
                    'user_id' => Auth::user()->id,

                ]);

                // Redirect back with success message
                return redirect()->route('admin.profile.edit')->with('success', 'Informations ajoutées avec succès');
    }

   public function show(GestionInformation $compte)
{
    return view('admin.compte.show', ['compte'=>$compte]);
}
public function edit(GestionInformation $compte)
{
    return view('admin.compte.edit', compact('compte'));
}
    public function update(Request $request, $id)
    {
        $compte = GestionInformation::findOrFail($id);
        $user = Auth::user();

        $validatedData = $request->validate([
            'bio' => 'required|string',
            'horaires' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'specialite' => 'required|string|max:255',
            'langue' => 'required|string|max:255',
            'diplome' => 'required|string|max:255',
            'experience' => 'required|string',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Ajoutez les règles de validation pour l'image si nécessaire

        ]);


        // Gérer le téléchargement de l'image
        if ($request->hasFile('avatar')) {
            $oldImagePath = $user->avatar;

            try {
                if (!empty($oldImagePath) && file_exists(public_path($oldImagePath))) {
                    unlink(public_path($oldImagePath));
                }
            } catch (\Exception $exception) {
                return redirect()->back()->withErrors(['avatar' => 'Image not found']);
            }

            $ext = $request->file('avatar')->extension();
            $file_name = date('YmdHis') . '.' . $ext;
            $request->file('avatar')->move(public_path('avatars'), $file_name);
            $user->avatar = 'avatars/' . $file_name;
        }


        // Sauvegarder les changements de l'utilisateur
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->save();

        $compte->update([
            'bio' => $validatedData['bio'],
            'horaires' => $validatedData['horaires'],
            'specialite' => $validatedData['specialite'],
            'langue' => $validatedData['langue'],
            'diplome' => $validatedData['diplome'],
            'experience' => $validatedData['experience'],
            'user_id' => Auth::user()->id,


        ]);


        return redirect()->route('admin.profile.edit')->with('success', 'inoformations mise à jour avec succès');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GestionInformation $compte)
    {
        $compte->delete();

        return redirect()->route('compte.index')->with('success', 'info compte supprime avec succes');
    }

}
