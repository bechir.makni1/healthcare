<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function storeProductRating(Request $request, $productId)
    {
        $request->validate([
            'rating' => 'required|integer|min:1|max:5',
        ]);

        $product = Product::findOrFail($productId);
        $product->ratings()->create([
            'user_id' => auth()->id(),
            'rating' => $request->rating,
        ]);

        return back()->with('status', 'merci pur votre evaluation.');
    }

    public function storeNutritionistRating(Request $request)
    {
        $nutritionistId = $request->input('nutritionist_id');

        // Vérifier si l'utilisateur est authentifié
        if (!auth()->check()) {
            return redirect()->back()->withErrors('status','Vous devez être connecté pour évaluer un nutritionniste.');
        }

        // Validate the incoming request data
        $request->validate([
            'rating' => 'required|integer|min:1|max:5',
        ]);

        try {
            // Find the nutritionist by ID
            $nutritionniste = User::findOrFail($nutritionistId);

            // Check if the user is indeed a nutritionist
            if ($nutritionniste->type !== User::TYPE_NUTRITIONNISTE) {
                return redirect()->back()->withErrors('status','The user is not a nutritionist.');
            }

            // Create a new rating record
            $nutritionniste->ratings()->create([
                'user_id' => auth()->id(),
                'rating' => $request->rating,
            ]);

            // Optionally, calculate and update average rating for the nutritionist
            $averageRating = $nutritionniste->ratings()->avg('rating');
            $nutritionniste->average_rating = $averageRating;
            $nutritionniste->save();

            // Redirect back with success message
            return redirect()->back()->with('status', 'merci pour votre evaluation.');
        } catch (\Exception $e) {
            // Handle any unexpected errors
            return redirect()->back()->withErrors('Failed to submit rating.');
        }
    }


}
