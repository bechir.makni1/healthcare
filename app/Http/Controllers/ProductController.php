<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $produits = Product::all()->sortByDesc('created_at');
        return view('admin.product.index', compact('produits'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'details' => 'required|string',
            'price' => 'required|numeric',
            'image' => 'image|max:2048',
            'livraison' => 'required|numeric',

        ]);

        $imagePath = null;

        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $file_name = date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images'), $file_name);
            $imagePath = 'images/' . $file_name;
        }


        $produit = Product::create([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'details' => $validatedData['details'],
            'image' => $imagePath,
            'price' => $validatedData['price'],
            'livraison' => $validatedData['livraison'],
            'user_id' => Auth::user()->id,
        ]);
        return redirect()->route('produit.index')->with('success', 'produit ajoutée avec succès');
    }

    public function show(Product $produit)
    {
        return view('admin.product.show', ['produit' => $produit]);
    }

    public function edit(Product $produit)
    {
        return view('admin.product.edit', compact('produit'));
    }

    public function update(Request $request, $id)
    {
        $produit = Product::findOrFail($id);
        $validatedData = $request->validate([
            'name' => ['string', 'max:255'],
            'description' => ['string', 'required'],
            'details' => ['string', 'required'],
            'price' => ['numeric', 'required'],
            'image' => ['image', 'mimes:jpg,png,jpeg', 'max:2048'],
        ]);

        $image = $produit->image;

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            // Supprimer l'ancienne image si elle existe
            if ($image && Storage::disk('public')->exists($image)) {
                Storage::disk('public')->delete($image);
            }

            // Enregistrer la nouvelle image
            $image = $request->file('image')->store('assets', 'public');
        }

        $produit->update([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'details' => $validatedData['details'],
            'price' => $validatedData['price'],
            'image' => $image,

        ]);

        return redirect()->route('produit.index')->with('success', 'Produit mise à jour avec succès');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $produit)
    {
        $produit->delete();

        return redirect()->route('produit.index')->with('success', 'Produit supprime avec succes');
    }
}
