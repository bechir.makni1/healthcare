<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ClientController extends Controller
{
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $clients = User::all()->where('type', User::TYPE_CLIENT)->sortByDesc('created_at');

        return view('admin.client.index', compact('clients'));

    }

    public function show($id)
    {
        $client = $this->userRepository->find($id);

        return view('admin.client.show')->with('client', $client);
    }

    public function create()
    {
        return view('admin.client.create');
    }

    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $profileImage = date('YmdHis') . "." . $avatar->getClientOriginalExtension();
            $avatar->storeAs('public/avatars', $profileImage);

            $data['avatar'] = $profileImage;
        }

        $this->userRepository->create($data);

        return redirect()->route('client.index')->with('success', 'Client ajouté avec succès');
    }

    public function edit($id)
    {
        $client = $this->userRepository->find($id);

        return view('admin.client.edit', compact('client'));
    }


    public function update(UpdateUserRequest $request, $id)
    {
        $data = $request->all();

        $client = $this->userRepository->find($id);

        $this->userRepository->update($data, $client->id);

        return redirect()->route('client.index')->with('success', 'Client modifié avec succès');
    }

    public function destroy($id)
    {
        $nclient = $this->userRepository->find($id);

        if (empty($client)) {
            return redirect(route('client.index')->with('Client not found'));
        }

        if ($this->userRepository->delete($client->id))
            return redirect(route('client.index')->with('success', 'Client supprimé avec succès'));

    }
}



