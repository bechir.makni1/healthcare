<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Availability;
use App\Repositories\AppointmentRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    public function __construct(AppointmentRepository $appointmentRepository)
    {
        $this->appointmentRepository = $appointmentRepository ;
    }

    public function index()
    {
        // Afficher les rendez-vous
        $appointments = Appointment::all()->sortByDesc('created_at');;
        return view('admin.appointments.index', compact('appointments'));
    }
    public function show()
    {
        return view('admin.appointments.appointment-list');
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'start_at' => 'required|date_format:H:i',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $date = $request->input('date');
        $startAt = $request->input('start_at');
        $nutritionistId = $request->input('nutritionist_id');

        // Vérifier la disponibilité du nutritionniste
        $appointmentExists = DB::table('appointments')
            ->where('nutritionist_id', $nutritionistId)
            ->where('date', $date)
            ->where('status', 'confirmed')
            ->where('start_at', $startAt)
            ->exists();

        if ($appointmentExists) {
            return redirect()->back()->with('error', 'Ce créneau horaire est déjà réservé pour ce nutritionniste.');
        }

        $input = $request->only(['date', 'start_at', 'nutritionist_id']);
        $input['user_id'] = Auth::id();
        $input['status'] = 'pending';

        $this->appointmentRepository->create($input);

        // Redirigez l'utilisateur ou renvoyez une réponse appropriée
        return redirect()->back()->with('success', 'Votre demande de rendez-vous a été envoyée avec succès!');
    }

    public function apiAppointments()
    {
        $appointments = Appointment::with('user')->get(['id', 'user_id', 'appointment_date']);
        $appointments = $appointments->map(fn($appointment) => [
            'title' => $appointment->user->name,
            'start' => $appointment->appointment_date,
            'end' => $appointment->appointment_date,
        ]);
        return response()->json($appointments);
    }
}
