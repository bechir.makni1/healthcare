<?php
namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\Product;
use App\Models\User;
use App\Services\TwilioService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;

class CommandeController extends Controller
{
    protected $twilioService;

    // Injection de dépendances pour obtenir une instance de TwilioService
    public function __construct(TwilioService $twilioService)
    {
        $this->twilioService = $twilioService;
    }
    public function index()
    {
        // Fetch cart items from session
        $cartItems = session()->get('cartItems', []);

        // Calculate subtotal
        $subtotal = array_sum(array_map(fn($item) => $item['price'] * $item['quantity'], $cartItems));

        // Calculate total livraison
        $totalLivraison = array_sum(array_map(fn($item) => $item['livraison'], $cartItems));

        // Calculate total (subtotal + livraison)
        $total = $subtotal + $totalLivraison;

        return view('front.caisse.index', compact('cartItems', 'total','totalLivraison','subtotal'));
    }

    public function store(Request $request)
    {
        // Validate the request
        $request->validate([
            'addresse' => 'required|string|max:255',
            'code_postal' => 'required|string|max:10',
            'pays' => 'required|string|max:255',
            'ville' => 'required|string|max:255',
            'telephone' => 'required|string|max:20|regex:/^\+?[1-9]\d{1,14}$/',
            'note' => 'nullable|string|max:255',
            'product_ids' => 'required|array',
            'product_ids.*' => 'exists:products,id',
            'quantities' => 'required|array',
            'quantities.*' => 'integer|min:1'
        ]);

        // Create a new Commande
        $commande = Commande::create([
            'addresse' => $request->addresse,
            'code_postal' => $request->code_postal,
            'pays' => $request->pays,
            'ville' => $request->ville,
            'telephone' => $request->telephone,
            'note' => $request->note,
            'user_id' => Auth::user()->id,
        ]);

        // Save associated products
        foreach ($request->product_ids as $index => $productId) {
          $a =  $commande->products()->attach($productId, ['quantity' => $request->quantities[$index]]);
        }
        // Clear the cart
        session()->forget('cartItems');

        // Redirect to a success page or order summary
        return redirect()->route('front.caisse.index')->with('status', 'Commande envoyée avec succès!');
    }
    public function Admin(){
     $user=Auth::user();
     $commandes=Commande::wherehas('products',function ($query)use($user){
        $query->where('user_id',$user->id);
     })->get();
        foreach ($commandes as $commande) {
            $total = 0;
            foreach ($commande->products as $product) {
                $quantity = $product->pivot->quantity;
                $price = $product->price;
                $livraison = $product->livraison;

                $total += $quantity * $price + $livraison;
            }
            $commande->total = $total;
        }
     return view('admin.commande.index',compact('commandes'));
    }
    public function destroy($id)
    {
        $commande = Commande::findOrFail($id);
        $commande->delete();

        return view('admin.commande.index')->with('success', 'Commande supprimée avec succès!');
    }
    public function show(Commande $id)
    {
        return view('admin.commande.show', ['commande' => $id]);
    }
    public function confirm($id)
    {
        $commande = Commande::findOrFail($id);

        // Logique de confirmation de la commande
        $commande->status = 'confirmed';
        $commande->save();

        // Préparation et envoi du SMS de confirmation
        $user = User::find($commande->user_id); // Utilisation de l'ID de l'utilisateur de la commande
        $message = "Votre commande a été confirmée. Merci pour votre confiance.";
        $this->twilioService->sendSmsToUser($commande, $message); // Utilisation de $this->twilioService
        return redirect()->route('commande.admin')->with('success', 'Commande confirmée et SMS envoyé.');
    }

    public function annuler($id)
    {
        $commande = Commande::findOrFail($id);

        // Logique de confirmation de la commande
        $commande->status = 'annuler';
        $commande->save();

        // Préparation et envoi du SMS de confirmation
        $user = User::find($commande->user_id); // Utilisation de l'ID de l'utilisateur de la commande
        $message = "Votre commande a été annuler a cause de repture de stock merci .";
        $this->twilioService->sendSmsToUser($commande, $message); // Utilisation de $this->twilioService
        return redirect()->route('commande.admin')->with('success', 'Commande confirmée et SMS envoyé.');
    }
}
