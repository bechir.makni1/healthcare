<?php

namespace App\Http\Controllers;

use App\Models\Conseil;
use App\Models\Product;
use App\Models\Recettes;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $conseils= Conseil::latest()->take(4)->get();
        $recettes= Recettes::latest()->take(8)->get();
        $categories = Category::latest()->take(4)->get();
        $nutritionnistes= User::where('type',User::TYPE_NUTRITIONNISTE)->latest()->take(4)->get();
        $produits= Product::latest()->take(6)->get();

        return view('welcome', compact('conseils','recettes','categories','nutritionnistes','produits'));
    }
}
