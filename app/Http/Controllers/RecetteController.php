<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Recettes;
use App\Models\User;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RecetteController extends Controller
{
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (Auth::check() && Auth::user()->type === User::TYPE_SUPER_ADMIN) {
            $recettes = Recettes::all()->sortByDesc('created_at');
        } else {
            $recettes = Recettes::where('user_id', Auth::user()->id)->orderByDesc('created_at')->get();
        }

        return view('recette.index', compact('recettes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('recette.create',
            [
                'categories' => Category::where('isActive', 1)->get()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'ingrédient' => 'required|string',
            'isActive' => 'required|boolean',
            'isComment' => 'required|boolean',
            'image' => 'nullable|image|max:2048',
            'category_id' => ['required', 'exists:categories,id'],
        ]);

        $imagePath = null;


        if ($request->hasFile('image')) {

            // Store new avatar
            $file = $request->file('image');
            $file_name = date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images'), $file_name);
            $imagePath = 'images/' . $file_name;
        }

        /*   if ($request->hasFile('image')) {
               $image = $request->file('image');
               $imagePath = $image->store('asset', 'public');
           }*/

        $tags = explode(',', $request->tags);

        $recette = Recettes::create([
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'ingrédient' => $validatedData['ingrédient'],
            'isActive' => $validatedData['isActive'],
            'isComment' => $validatedData['isComment'],
            'image' => $imagePath,
            'category_id' => $validatedData['category_id'],
            'user_id' => Auth::user()->id,
        ]);
        $recette->tag($tags);
        return redirect()->route('recette.index')->with('success', 'Recette ajoutée avec succès');
    }


    /**
     * Display the specified resource.
     */
    public function show(Recettes $recette)
    {
        return view('recette.show', ['recette' => $recette]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Recettes $recette)
    {
        $categories = Category::where('isActive', 1)->get();
        return view('recette.edit', compact('recette', 'categories'));
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $recette = Recettes::findOrFail($id);


        $validatedData = $request->validate([
            'title' => ['string', 'max:255'],
            'description' => ['string', 'required'],
            'image' => ['image', 'nullable', 'mimes:jpg,png,jpeg', 'max:2048'],
            'is_active' => ['required', 'boolean'],
            'is_comment' => ['required', 'boolean'],
            'is_shareable' => ['required', 'boolean'],
            'category_id' => ['required', 'exists:categories,id'],
            'tags' => ['string', 'nullable']
        ]);

        $image = $recette->image;

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            // Store new avatar
            $file = $request->file('image');
            $file_name = date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images'), $file_name);
            $imagePath = 'images/' . $file_name;

        }

        $recette->update([
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'is_active' => $validatedData['is_active'],
            'is_shareable' => $validatedData['is_shareable'],
            'is_comment' => $validatedData['is_comment'],
            'image' => $imagePath ?? $recette->image,
            'category_id' => $validatedData['category_id'],
            'tags' => $validatedData['tags'],

        ]);

        // Mise à jour des tags
        $tags = explode(',', $request->tags);
        $recette->retag($tags);

        return redirect()->route('recette.index')->with('success', 'Recette mise à jour avec succès');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Recettes $recette)
    {
        $recette->delete();

        return redirect()->route('recette.index')->with('success', 'Recette supprimé avec succes');
    }

    public function comment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
        ]);

        $recette = Recettes::findOrFail($id);


        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Il y a des erreurs de validation.');
        }

        // Si la validation réussit, continuez à créer le commentaire
        $data = $validator->validated();

        $data = [
            'recettes_id' => $recette->id,
            'content' => $request->get('content'),
            'user_id' => Auth::id()
        ];
        $comments = $this->commentRepository->create($data);

        // Passez la recette, la catégorie et le message de succès à la vue
        return redirect()->back()->with('success', 'Commentaire ajouté avec succès!')->with(compact('recette', 'comments'));
    }
}
