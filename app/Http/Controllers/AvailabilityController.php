<?php

namespace App\Http\Controllers;

use App\Models\Availability;
use Illuminate\Http\Request;

class AvailabilityController extends Controller
{
    public function index()
    {
        // Afficher les disponibilités
        $availabilities = Availability::all();
        return view('availabilities.index', compact('availabilities'));
    }

    public function create()
    {
        // Afficher le formulaire pour ajouter une disponibilité
        return view('availabilities.create');
    }

    public function store(Request $request)
    {
        // Enregistrer la disponibilité
        $availability = new Availability();
        $availability->nutritionist_id = auth()->id();
        $availability->available_date = $request->available_date;
        $availability->is_available = true;
        $availability->save();

        return redirect()->route('availabilities.index')->with('success', 'Disponibilité ajoutée avec succès');
    }
}
