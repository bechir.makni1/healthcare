<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user){
                return redirect()->route('welcome');
            }

            /*if ($user->type === User::TYPE_CLIENT ) {
                return redirect()->route('welcome');
            } elseif ($user->type === User::TYPE_SUPER_ADMIN || $user->type === User::TYPE_NUTRITIONNISTE) {
                return redirect()->route('admin.dashboard');
            }*/
        }

        return redirect()->back()->withErrors(['email' => 'You have entered invalid credentials']);
    }
    public function logout(Request $request):RedirectResponse
    {
        Auth::guard('')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        session()->forget('cartItems');

        return redirect('/login');
    }
}






