<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Conseil;
use App\Models\Recettes;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $nutritionniste_recettes = null;
        $nutritionniste_conseils=null;
        if ($user->type === User::TYPE_NUTRITIONNISTE) {
            $nutritionniste_recettes = Recettes::where('user_id', $user->id)->count();
            $nutritionniste_conseils = Conseil::where('user_id', $user->id)->count();
        }

        $recettes = Recettes::all();
        $conseils = Conseil::all();
        $recettes_recentes = Recettes::where('isActive',1)->orderBy('created_at', 'DESC')->take(10)->get();
        $categories = Category::count();

        return view('admin.dashboard', [
            'nutritionniste_recettes' => $nutritionniste_recettes,
            'nutritionniste_conseils'=> $nutritionniste_conseils,
            'recettes' => $recettes,
            'conseils' => $conseils,
            'recettes_recentes' => $recettes_recentes,
            'categories' => $categories,
        ]);
    }
}
