<?php

namespace App\Http\Requests\recette;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRecetteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['string', 'max:255'],
            'ingrédient' => ['string', 'required'],
            'description' => ['string', 'required'],
            'image' => ['image', 'nullable', 'mimes:jpg,png,jpeg', 'max:2048'],
            'is_active'=>['required'],
            'is_shareable'=>['required'],
            'is_comment'=>['required'],
            'category_id'=>['required'],
        ];
    }
}
